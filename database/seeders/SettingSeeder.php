<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = Setting::firstOrCreate(
            [
                'key' => 'history_group',
            ],
            [
                'name' => 'History group ID',
                'description' => '',
                'visibility' => 1,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'report_group',
            ],
            [
                'name' => 'Report group ID',
                'description' => '',
                'visibility' => 1,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'managers_group',
            ],
            [
                'name' => 'Managers group ID (/managerschat)',
                'description' => '',
                'visibility' => 1,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'fleetteam_group',
            ],
            [
                'name' => 'ID группы отчетов',
                'description' => '',
                'visibility' => 1,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'fuel_price_per_mile',
            ],
            [
                'name' => 'Fuel price per mile',
                'description' => '0.70',
                'visibility' => 1,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'toll_price_per_mile',
            ],
            [
                'name' => 'Toll price per mile',
                'description' => '0.05',
                'visibility' => 1,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'temporary_testing_group',
            ],
            [
                'name' => 'Temporary testing group',
                'description' => '',
                'visibility' => 0,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'dispatch_warnings_text',
            ],
            [
                'name' => 'Dispatch warnings text',
                'description' => 'Late PU/Del charge:' . PHP_EOL . 'Macro point//Relay app:' . PHP_EOL . 'No Update charge - 150$' . PHP_EOL . 'Unapproved Bobtail-in-out - 1000$;' . PHP_EOL . 'Failure to send trailer photos as required - 100$;',
                'visibility' => 0,
            ]
        );
        $setting = Setting::firstOrCreate(
            [
                'key' => 'dispatch_confirmation_text',
            ],
            [
                'name' => 'Dispatch confirmation text',
                'description' => '✅ Called on the phone, explained all details: stops and all load requirements;' . PHP_EOL . '✅ Dispatched the driver and provided trailer information;' . PHP_EOL . '✅ PCS Load created and dispatched, Dashboard updated;' . PHP_EOL . '✅ The driver confirmed all details on the phone.',
                'visibility' => 0,
            ]
        );
    }
}
