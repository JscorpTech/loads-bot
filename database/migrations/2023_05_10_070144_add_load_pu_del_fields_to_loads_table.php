<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLoadPuDelFieldsToLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->decimal('mile', 12, 2)->default(0);
            $table->text('pickup_location')->nullable();
            $table->timestamp('pickup_time')->nullable();
            $table->text('delivery_location')->nullable();
            $table->timestamp('delivery_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('loads', function (Blueprint $table) {
            $table->dropColumn(['mile', 'pickup_location', 'pickup_time', 'delivery_location', 'delivery_time', 'driver_id']);
        });
    }
}
