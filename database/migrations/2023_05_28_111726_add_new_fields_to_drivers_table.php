<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewFieldsToDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->decimal('weekly_target')->default(10000);
            $table->integer('rest_time')->default(180);
            $table->string('language', 3)->default('en');
            $table->string('ownership', 60)->nullable();
            $table->string('religion', 60)->nullable();
            $table->string('nationality', 60)->nullable();
            $table->timestamp('birthday')->nullable();
            $table->string('holidays', 60)->nullable();
            $table->string('group_chat_id', 32)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('drivers', function (Blueprint $table) {
            $table->dropColumn(['weekly_target', 'rest_time', 'language', 'ownership', 'religion', 'nationality', 'birthday', 'holidays']);
        });
    }
}
