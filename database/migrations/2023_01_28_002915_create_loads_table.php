<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loads', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('dsp_bot_user_id')->nullable();
            $table->bigInteger('qc_bot_user_id')->nullable();
            $table->tinyInteger('type')->default(0);
            $table->string('load_id')->nullable();
            $table->string('driver')->nullable();
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->string('pcs_number')->nullable();
            $table->tinyInteger('file_type')->default(0);
            $table->text('file_id')->nullable();
            $table->decimal('total', 12, 2)->default(0);
            $table->tinyInteger('status')->default(0);
            $table->string('pickup_ontime', 10)->nullable();
            $table->text('pickup_ontime_comment')->nullable();
            $table->string('delivery_ontime', 10)->nullable();
            $table->text('delivery_ontime_comment')->nullable();
            $table->decimal('dsp_bonus', 8, 2)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loads');
    }
}
