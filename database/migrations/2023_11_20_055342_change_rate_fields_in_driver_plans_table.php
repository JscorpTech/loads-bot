<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeRateFieldsInDriverPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->decimal('total', 12, 2)->default(0)->after('target_mile');
            $table->renameColumn('total_rate', 'rate');
            $table->renameColumn('cancel_total_rate', 'tonu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->renameColumn('rate', 'total_rate');
            $table->renameColumn('tonu', 'cancel_total_rate');
            $table->dropColumn('total');
        });
    }
}
