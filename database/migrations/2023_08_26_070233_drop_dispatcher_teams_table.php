<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DropDispatcherTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('dispatcher_teams');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('dispatcher_teams', function (Blueprint $table) {
            $table->id();
            $table->foreignId('team_id')->constrained()->cascadeOnDelete();
            $table->bigInteger('dispatcher_id');
            $table->integer('sort_number')->default(0);
            $table->timestamps();

            $table->foreign('dispatcher_id')->references('id')->on('bot_user')->cascadeOnDelete();
        });
    }
}
