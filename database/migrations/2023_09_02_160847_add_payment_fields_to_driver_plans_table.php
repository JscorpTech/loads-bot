<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaymentFieldsToDriverPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->decimal('pay_per_mile')->default(0);
            $table->decimal('penske_pay_per_mile')->default(0);
            $table->decimal('fixed_cost')->default(0);
            $table->decimal('fuel_price_per_mile')->default(0);
            $table->decimal('toll_price_per_mile')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->dropColumn(['pay_per_mile', 'penske_pay_per_mile', 'fixed_cost', 'fuel_price_per_mile', 'toll_price_per_mile']);
        });
    }
}
