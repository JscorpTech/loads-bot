<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompletedFieldsToDriverPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->decimal('target_mile', 12, 2)->default(0);
            $table->decimal('total_rate', 12, 2)->default(0);
            $table->decimal('cancel_total_rate', 12, 2)->default(0);
            $table->decimal('total_mile', 12, 2)->default(0);
            $table->decimal('total_deadhead_mile', 12, 2)->default(0);
            $table->decimal('per_mile', 12, 2)->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('driver_plans', function (Blueprint $table) {
            $table->dropColumn(['total_rate', 'cancel_total_rate', 'total_mile', 'total_deadhead_mile', 'per_mile']);
        });
    }
}
