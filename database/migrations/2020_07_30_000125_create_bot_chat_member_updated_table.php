<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBotChatMemberUpdatedTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bot_chat_member_updated', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('chat_id');
            $table->bigInteger('user_id');
            $table->timestamp('date')->nullable();
            $table->text('old_chat_member')->nullable();
            $table->text('new_chat_member')->nullable();
            $table->text('invite_link')->nullable();
            $table->timestamps();

            $table->foreign('chat_id')->references('id')->on('bot_chat')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('bot_user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bot_chat_member_updated');
    }
}
