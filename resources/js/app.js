require('./bootstrap')
window.AirDatepicker = require('air-datepicker/air-datepicker.js')
require('bootstrap-select')
window.Dropzone = require("dropzone")
window.Draggable = require('@shopify/draggable')
// window.SimpleMDE = require('simplemde/dist/simplemde.min.js')
// window.Quill = require('quill')
// window.QuillDeltaToHtmlConverter = require('quill-delta-to-html').QuillDeltaToHtmlConverter;

// window.ClassicEditor = require( '@ckeditor/ckeditor5-build-classic' );
// import ClassicEditor from '@ckeditor/ckeditor5-editor-classic/src/classiceditor';
// import Underline from '@ckeditor/ckeditor5-basic-styles/src/underline';
// import Strikethrough from '@ckeditor/ckeditor5-basic-styles/src/strikethrough';

require('../js/ckeditor');
require('@fortawesome/fontawesome-free/js/solid');
require('@fortawesome/fontawesome-free/js/regular');
require('@fortawesome/fontawesome-free/js/fontawesome');

$('[data-toggle="tooltip"]').tooltip()
$('[data-toggle="popover"]').popover()
$('.copy-btn').popover({
    // trigger: 'manual'
})

// datepicker, timepicker
let datepickers = document.querySelectorAll('.datepicker-standard');
for (let datepicker of datepickers) {
    new AirDatepicker(datepicker, {
        startDate: datepicker.value,
        dateFormat: 'yyyy-MM-dd'
    });
}
let datetimepickers = document.querySelectorAll('.datetimepicker-standard');
for (let datetimepicker of datetimepickers) {
    new AirDatepicker(datetimepicker, {
        startDate: datetimepicker.value,
        timepicker: true,
        dateFormat: 'yyyy-MM-dd',
        timeFormat: 'HH:mm'
    });
}
let timepickers = document.querySelectorAll('.only-timepicker-input');
for (let timepicker of timepickers) {
    let hours = timepicker.dataset.hours;
    let minutes = timepicker.dataset.minutes;
    let startDate = new Date();
    startDate.setHours(hours);
    startDate.setMinutes(minutes);
    new AirDatepicker(timepicker, {
        startDate,
        onlyTimepicker: true,
        timepicker: true,
        timeFormat: 'HH:mm',
        inline: true
    });
}

// $('.daterangepicker').datepicker({
//     range: true
// });

// // markdown editor
// let mdTextareas = document.querySelectorAll('.simplemde-textarea');
// for (mdTextarea of mdTextareas) {
//     new SimpleMDE({
//         element: mdTextarea,
//         toolbar: ["bold", "italic", "strikethrough", "link", "|", "preview"],
//         // hideIcons: ['guide', 'heading', 'quote', 'unordered-list', 'ordered-list', 'image', 'side-by-side', 'fullscreen'],
//         // showIcons: ['strikethrough'],
//         // blockStyles:{
//         //     bold: "*",
//         //     italic: "_"
//         // },
//     });
// }

// // html quill editor
// let quillEditors = document.querySelectorAll('.quill-editor');
// for (quillEditor of quillEditors) {
//     let quill = new Quill(quillEditor, {
//         // theme: 'bubble'
//         modules: {
//             toolbar: [
//                 ['bold', 'italic', 'underline', 'strike', 'link'],
//             ]
//         },
//         theme: 'snow'
//     });
//     quill.on('text-change', function() {
//         let delta = quill.getContents();
//         let converter = new QuillDeltaToHtmlConverter(delta.ops, {
//             paragraphTag: ''
//         });
//         let html = converter.convert();
//         let elemID = quillEditor.dataset.id;
//         document.getElementById(elemID).value = html;
//     });
// }

// html ckeditor
let ckEditors = document.querySelectorAll('.ckeditor');
for (let ckEditor of ckEditors) {
    ClassicEditor
        .create(ckEditor, {
            toolbar: ['bold', 'italic', 'strikethrough', 'underline', 'link']
        });
    //     ClassicEditor
    //         .create(ckEditor, {
    //             plugins: [Underline, Strikethrough],
    //             toolbar: [ 'bold', 'italic', 'strikethrough', 'underline', 'link' ]
    //         });
}



// theme
$('body').on('click', '.uploaded-file-one-remove-btn', function () {
    $(this).closest('.uploaded-file-one').remove();
})

$('.copy-btn').on('click', function (e) {
    e.preventDefault();
    let target = $(this).data('target');
    navigator.clipboard.writeText($(target).text()).then(
        () => {
            // console.log('success')
        },
        () => {
            // console.log('error')
        },
    );
    setTimeout(() => {
        $(this).popover('hide')
    }, 1000);
})

$('.edit-table-info-btn').on('click', function (e) {
    let targetSelector = $(this).data('target')
    let target = $(targetSelector)
    let modal = $('#edit-table-info-modal')
    modal.find('.form-result').empty()
    let textarea = modal.find('textarea')
    modal.find('input[name="table"]').val(target.data('table'))
    modal.find('input[name="field"]').val(target.data('field'))
    modal.find('input[name="id"]').val(target.data('id'))
    modal.find('input[name="target_selector"]').val(targetSelector)
    textarea.val(target.text().trim())
    modal.modal('show')
    textarea.trigger('focus')
})

$('.edit-table-info-form').on('submit', function (e) {
    e.preventDefault()
    let form = $(this)
    let formResult = form.find('.form-result')
    formResult.empty()
    let btn = form.find('[type="submit"]')
    let url = form.attr('action')
    const sendData = new URLSearchParams();
    for (const pair of new FormData(form[0])) {
        sendData.append(pair[0], pair[1]);
    }
    btn.append('<i class="fa fa-spin fa-circle-notch"></i>')
    btn.prop('disabled', true)
    btn.addClass('disabled')
    fetch(url, {
        method: 'post',
        body: sendData,
        headers: {
            "Accept": "application/json",
        },
    })
        .then(res => res.json())
        .then(data => {
            console.log(data)
            if (data.message) {
                formResult.text(data.message)
            }
            if (data.status == 'success') {
                let descr = form.find('[name="description"]')
                $(form.find('[name="target_selector"]').val()).text(descr.val())
                descr.val('')
                $('#edit-table-info-modal').modal('hide')
            }
        })
        .catch(data => {
            console.log('fail')
        })
        .finally(() => {
            btn.find('.fa-circle-notch').remove()
            btn.prop('disabled', false)
            btn.removeClass('disabled')
        })
})

function showScrollToTop() {
    if (window.scrollY > window.outerHeight) {
        $("#to-top-btn").addClass('d-flex').removeClass('d-none');
    } else {
        $("#to-top-btn").addClass('d-none').removeClass('d-flex');
    }
}
showScrollToTop()
$(window).on("scroll", showScrollToTop);
$('#to-top-btn').on('click', function () {
    $("html, body").animate({ scrollTop: 0 }, 500);
})
