@extends('layouts.app')

@section('title', __('Confirm edit - Load') . '#' . $load->load_id)

@php
    $sessionLoadData = $sessionLoad['data'];
@endphp

@section('content')
    <div class="container">

        @include('partials.alerts')

        <h1>
            {{ __('Confirm edit - Load') }} {{ $sessionLoadData['load_id'] }}
        </h1>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <tr>
                        <td>
                            <strong>{{ __('Company code') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['company_code'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Load ID') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['load_id'] }}
                        </td>
                    </tr>
                    @if (!$load->isThirdParty() && $driver)
                    <tr>
                        <td>
                            <strong>{{ __('Driver') }}</strong>
                        </td>
                        <td>
                            {{ $driver->name }}
                        </td>
                    </tr>
                    @endif
                    @if ($load->isThirdParty())
                    <tr>
                        <td>
                            <strong>{{ __('Third company') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['third_company'] }}
                        </td>
                    </tr>
                    @endif
                    <tr>
                        <td>
                            <strong>{{ __('PCS number') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['pcs_number'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Rate') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['rate'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Authorized loaded mile') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['authorized_loaded_mile'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Load DH mile') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['deadhead_mile'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Authorized mile (PLD, TRL, FLT miles)') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['authorized_mile'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Unauthorized miles (including Home DH miles)') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['unauthorized_mile'] }}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <strong>{{ __('PU address (full)') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['pickup_location'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('PU timezone') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['pickup_timezone'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('PU time') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['local_pickup_time'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Delivery address (full)') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['delivery_location'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Delivery timezone') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['delivery_timezone'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Delivery time') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['local_delivery_time'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Special note') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['special_note'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Check in as') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['check_in_as'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Trailer') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['trailer'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Trailer note') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['trailer_note'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Pickup address and time (2, 3, 4 Stops)') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['pickup_additional_stops'] }}
                        </td>
                    </tr>
                    @if ($user->isQc())
                    <tr>
                        <td>
                            <strong>{{ __('Pickup status') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['pickup_status'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Pickup QC comment') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['pickup_qc_comment'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Delivery status') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['delivery_status'] }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>{{ __('Delivery QC comment') }}</strong>
                        </td>
                        <td>
                            {{ $sessionLoadData['delivery_qc_comment'] }}
                        </td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>

        <form action="{{ route('loads.edit.confirm', [$load->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div>
                <button type="submit" class="btn btn-primary">{{ __('Confirm') }}</button>
            </div>
        </form>

    </div>
@endsection
