@extends('layouts.app')

@section('title', __('Loads'))

@section('content')
    <div class="container-fluid">

        @include('partials.alerts')

        <h1>{{ __('Loads') }}</h1>

        <form action="{{ route('loads.index') }}" id="load-filter-form">

            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="created_time_from">{{ __('Created time from') }}</label>
                        <input type="text" name="created_time_from" class="form-control datepicker-standard"
                            value="{{ is_object($filter['created_time_from']) ? $filter['created_time_from']->format('Y-m-d') : $filter['created_time_from'] }}">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="created_time_to">{{ __('Created time to') }}</label>
                        <input type="text" name="created_time_to" class="form-control datepicker-standard"
                            value="{{ is_object($filter['created_time_to']) ? $filter['created_time_to']->format('Y-m-d') : $filter['created_time_to'] }}">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="pickup_time_from">{{ __('PU time from') }}</label>
                        <input type="text" name="pickup_time_from" class="form-control datepicker-standard"
                            value="{{ $filter['pickup_time_from'] ? $filter['pickup_time_from']->format('Y-m-d') : '' }}">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="pickup_time_to">{{ __('PU time to') }}</label>
                        <input type="text" name="pickup_time_to" class="form-control datepicker-standard"
                            value="{{ $filter['pickup_time_to'] ? $filter['pickup_time_to']->format('Y-m-d') : '' }}">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="dsp_bot_user_id">{{ __('DSP') }}</label>
                        <select class="form-control selectpicker" data-live-search="true" name="dsp_bot_user_id"
                            id="dsp_bot_user_id">
                            <option value="-">-</option>
                            @foreach ($dsps as $dsp)
                                <option value="{{ $dsp->user_id }}" @if ($dsp->user_id == $filter['dsp_bot_user_id']) selected @endif>
                                    {{ $dsp->full_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="qc_bot_user_id">{{ __('QC') }}</label>
                        <select class="form-control selectpicker" data-live-search="true" name="qc_bot_user_id"
                            id="qc_bot_user_id">
                            <option value="-">-</option>
                            @foreach ($qcs as $qc)
                                <option value="{{ $qc->user_id }}" @if ($qc->user_id == $filter['qc_bot_user_id']) selected @endif>
                                    {{ $qc->full_name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="load_id">{{ __('Load ID') }}</label>
                        <input type="text" name="load_id" class="form-control" value="{{ $filter['load_id'] }}">
                    </div>
                </div>

                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="driver_id">{{ __('Driver') }}</label>
                        <select class="form-control selectpicker" data-live-search="true" name="driver_id" id="driver_id">
                            <option value="-">-</option>
                            @foreach ($drivers as $driver)
                                <option value="{{ $driver->id }}" @if ($driver->id == $filter['driver_id']) selected @endif>
                                    {{ $driver->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="pcs_number">{{ __('PCS number') }}</label>
                        <input type="text" name="pcs_number" class="form-control" value="{{ $filter['pcs_number'] }}">
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="shift">{{ __('Shift') }}</label>
                        <select class="form-control" name="shift" id="shift">
                            <option value="-">-</option>
                            @foreach ($shifts as $value)
                                <option value="{{ $value }}" @if ((string) $value == $filter['shift']) selected @endif>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="type">{{ __('Type') }}</label>
                        <select class="form-control" name="type" id="type">
                            <option value="-">-</option>
                            @foreach ($types as $key => $value)
                                <option value="{{ $key }}" @if ((string) $key == $filter['type']) selected @endif>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="status">{{ __('Status') }}</label>
                        <select class="form-control" name="status" id="status">
                            <option value="-">-</option>
                            @foreach ($statuses as $key => $value)
                                <option value="{{ $key }}" @if ((string) $key == $filter['status']) selected @endif>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="pickup_status">{{ __('Pickup status') }}</label>
                        <select name="pickup_status" class="form-control">
                            <option value="-">-</option>
                            <option value="Ontime" @if ($filter['pickup_status'] == 'Ontime') selected @endif>Ontime</option>
                            <option value="Late" @if ($filter['pickup_status'] == 'Late') selected @endif>Late</option>
                        </select>
                    </div>
                </div>
                <div class="col-6 col-md-3">
                    <div class="form-group">
                        <label for="delivery_status">{{ __('Delivery status') }}</label>
                        <select name="delivery_status" class="form-control">
                            <option value="-">-</option>
                            <option value="Ontime" @if ($filter['delivery_status'] == 'Ontime') selected @endif>Ontime</option>
                            <option value="Late" @if ($filter['delivery_status'] == 'Late') selected @endif>Late</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button class="btn btn-primary" type="submit">{{ __('Apply filter') }}</button>
            </div>
        </form>

        <div class="my-4">
            <div class="my-2">
                <a href="{{ route('loads.download', request()->all()) }}"
                    class="btn btn-sm btn-success">{{ __('Download') }} XLSX</a>
            </div>
        </div>

        @if ($stats)
            <div class="my-4">
                <h4>Stats</h4>
                <div class="my-2">
                    <form action="{{ route('loads.report', request()->all()) }}">
                        @csrf
                        <button type="submit" class="btn btn-sm btn-success">{{ __('Send report') }}</button>
                    </form>
                </div>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-sm">
                        <tr class="table-active">
                            <th>DSP</th>
                            @foreach (\App\Models\Load::types() as $key => $value)
                                <th>{{ $value }}</th>
                                <th>{{ $value }} Cancelled</th>
                            @endforeach
                            <th>Total Loads</th>
                            <th>Total Loads Cancelled</th>
                        </tr>
                        @foreach ($stats as $stat)
                            <tr>
                                <td>{{ $stat['dsp']->full_name }}</td>
                                @foreach (\App\Models\Load::types() as $key => $value)
                                    <td>{{ $stat['by_load_types'][$key]['loads'] }}</td>
                                    <td>{{ $stat['by_load_types'][$key]['loads_cancelled'] }}</td>
                                @endforeach
                                <td>{{ $stat['total_loads'] }}</td>
                                <td>{{ $stat['total_loads_cancelled'] }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        @endif

        <div class="my-4">
            <h4>{{ __('Loads') }}</h4>
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('Company code') }}
                        </th>
                        <th>
                            {{ __('Load ID') }}
                        </th>
                        <th>
                            {{ __('Type') }}
                        </th>
                        {{-- <th>
                            {{ __('Status') }}
                        </th> --}}
                        <th>
                            {{ __('Driver') }} / {{ __('Third company') }}
                        </th>
                        <th>
                            {{ __('PCS number') }}
                        </th>
                        <th>
                            {{ __('Pickup / Delivery') }}
                        </th>
                        <th>
                            {{ __('Pickup / Delivery ontime') }}
                        </th>

                        <th>
                            {{ __('Total $') }}
                        </th>
                        <th>
                            {{ __('Total mile') }}
                        </th>

                        <th>
                            {{ __('Special note') }}
                        </th>
                        <th>
                            {{ __('DSP') }}
                        </th>
                        <th>
                            {{ __('Assigned DSP') }}
                        </th>
                        <th>
                            {{ __('QC') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($loads as $load)
                        <tr>
                            <td>
                                {{ $load->company_code }}
                            </td>
                            <td class="text-center">
                                <strong>{{ $load->load_id }}</strong>
                                <br>
                                <em class="text-info">{{ Helper::formatDateTime($load->created_at) }}</em>
                                <br>
                                <em class="text-success">{{ Helper::formatDateTime($load->updated_at) }}</em>
                            </td>
                            <td>
                                {{ $load->type_title }}
                            </td>
                            {{-- <td>
                                {{ $load->status_title }}
                            </td> --}}
                            <td>
                                {{ $load->isThirdParty() ? $load->third_company : $load->driver_name }}
                            </td>
                            <td>
                                {{ $load->pcs_number }}
                            </td>
                            <td>
                                <div class="mb-1">
                                    PU: {{ $load->pickup_location }} <br>
                                    <i>{{ $load->pickup_time }}</i>
                                </div>
                                <div>
                                    Del: {{ $load->delivery_location }} <br>
                                    <i>{{ $load->delivery_time }}</i>
                                </div>
                            </td>
                            <td class="text-nowrap">
                                {{ $load->pickup_status }} / {{ $load->delivery_status }}
                            </td>

                            <td>
                                {{ $load->total }}
                            </td>
                            <td>
                                {{ $load->mile }}
                            </td>

                            <td>
                                {{ $load->special_note }}
                            </td>
                            <td>
                                {{ $load->dsp_full_name }}
                                {{-- <br>
                                {{ $load->dsp_bonus }} % --}}
                            </td>
                            <td>
                                {{ $load->assigned_dsp_full_name }}
                            </td>
                            <td>
                                {{ $load->qc_full_name }}
                            </td>
                            <td class="text-nowrap">
                                <a href="{{ route('loads.show', ['load' => $load->id]) }}"
                                    class="btn btn-sm btn-info">{{ __('View more') }}</a>
                                @can('update-load', $load)
                                    <a href='{{ route('loads.edit', [$load->id]) }}' class='text-info'
                                        target='_blank'>{{ __('Edit') }}</a>
                                @endcan
                                @can('cancel-load', $load)
                                    <a href='{{ route('loads.cancel', [$load->id]) }}' class='text-danger'
                                        target='_blank'>{{ __('Cancel') }}</a>
                                @endcan
                                @can('delete-load', $load)
                                    <a href="javascript:;"
                                        onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-load-{{ $load->id }}').submit() }"
                                        class="text-danger">{{ __('Delete') }}</a>
                                    <form action="{{ route('loads.destroy', [$load->id]) }}" method="post"
                                        id="delete-load-{{ $load->id }}">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                @endcan
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="10" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            {{ $loads->withQueryString()->links() }}
        </div>

        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection

@section('scripts')
    <script>
        //
    </script>
@endsection
