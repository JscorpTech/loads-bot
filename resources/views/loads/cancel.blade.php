@extends('layouts.app')

@section('title', __('Cancel load'))

@section('content')
    <div class="container">

        <h1>{{ __('Cancel load') }} <a href="{{ route('loads.show', [$load->id]) }}" target="_blank">{{ $load->load_and_pcs }}</a></h1>

        <form action="{{ route('loads.cancel', [$load->id]) }}" method="POST">

            @csrf

            <div class="form-group">
                <label for="form_tonu">{{ __('TONU') }}</label>
                <input type="number" name="tonu" id="form_tonu" class="form-control" value="{{ old('tonu') ?? 0 }}" required>
                @error('tonu')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_cancel_time">{{ __('Cancel time (east time yyyy-mm-dd hh:mm)') }}</label>
                <input type="text" name="cancel_time" id="form_cancel_time"
                    class="form-control datetimepicker-standard"
                    value="{{ old('cancel_time') }}" required>
                @error('cancel_time')
                    <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <label for="form_cancel_comment">{{ __('Comment') }}</label>
                <textarea type="number" name="cancel_comment" id="form_cancel_comment" class="form-control" required>{{ old('cancel_comment') }}</textarea>
                @error('cancel_comment')
                <div class="invalid-feedback d-block">{{ $message }}</div>
                @enderror
            </div>

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
