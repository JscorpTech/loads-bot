<div class="form-group">
    <label for="form_name">{{ __('Name') }}</label>
    <input type="text" name="name" id="form_name" class="form-control" value="{{ old('name') ?? $user->name }}" required>
    @error('name')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_email">{{ __('E-mail') }}</label>
    <input type="email" name="email" id="form_email" class="form-control" value="{{ old('email') ?? $user->email }}" required>
    @error('email')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_name">{{ __('New password') }}</label>
    <input type="password" name="password" id="form_password" class="form-control" value="" @if(!$user->exists) required @endif>
    @error('password')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="bot_user_info_id">{{ __('Bot user') }}</label>
    <select name="bot_user_info_id" id="bot_user_info_id" class="form-control">
        @php
            $currentValue = old('bot_user_info_id') ?? $user->bot_user_info_id;
        @endphp
        <option value="">-</option>
        @foreach($botUsers as $botUser)
            <option value="{{ $botUser->botUserInfo->id }}" @if($botUser->botUserInfo->id == $user->bot_user_info_id) selected @endif>{{ $botUser->botUserInfo->long_name }}</option>
        @endforeach
    </select>
    @error('bot_user_info_id')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="role_id">{{ __('Role') }}</label>
    <select name="role_id" id="role_id" class="form-control">
        @php
            $currentValue = old('role_id') ?? $user->role_id;
        @endphp
        <option value="">-</option>
        @foreach($roles as $key => $value)
            <option value="{{ $key }}" @if($key == $user->role_id) selected @endif>{{ $value }}</option>
        @endforeach
    </select>
    @error('role_id')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isActive = old('active') ?? $user->active;
    @endphp
    <label for="active" class="mb-0">{{ __('User is active') }}</label>
    <div>
        <input type="checkbox" name="active" id="active"  @if($isActive) checked @endif >
    </div>
    @error('active')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>
