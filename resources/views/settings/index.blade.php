@extends('layouts.app')

@section('title', __('Settings'))

@section('content')
    <div class="container">

        @include('partials.alerts')

        {{-- <div class="my-4">
            <a href="{{ route('settings.create') }}" class="btn btn-lg btn-info">
                {{ __('Add setting') }}
            </a>
        </div> --}}

        <h1>{{ __('Settings') }}</h1>

        <div class="my-4">
            <h3>{{ __('Settings') }}</h3>
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('Key') }}
                        </th>
                        <th>
                            {{ __('Title') }}
                        </th>
                        <th>
                            {{ __('Description') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($settings as $setting)
                        <tr>
                            <td>{{ $setting->key }}</td>
                            <td>{{ $setting->name }}</td>
                            <td>{{ $setting->description }}</td>
                            <td class="text-nowrap">
                                <a href="{{ route('settings.edit', ['setting' => $setting->id]) }}" class="btn btn-sm btn-warning">{{ __('Edit') }}</a>
                                {{-- <a href="#" onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-setting-{{ $setting->id }}').submit() }" class="btn btn-sm btn-danger">{{ __('Delete') }}</a>
                                <form action="{{ route('settings.destroy', ['setting' => $setting->id]) }}" method="post" id="delete-setting-{{ $setting->id }}">
                                    @csrf
                                    @method('DELETE')
                                </form> --}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" class="text-center">{{ __('Nothing fdound') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
