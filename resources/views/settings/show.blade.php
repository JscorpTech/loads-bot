@extends('layouts.app')

@section('title', __('Setting') . '#' . $setting)
@section('body_class', '')

@section('content')
    <div class="container">

    	<h1>
    		{{ __('Setting') }} #{{ $setting }}
    	</h1>

        <div class="py-4 d-print-none">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('settings.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

    </div>
@endsection
