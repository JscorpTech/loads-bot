@extends('layouts.app')

@section('title', __('Add setting'))

@section('content')
    <div class="container">

        <h1>{{ __('Add setting') }}</h1>

        <div class="py-4">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('settings.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('settings.store') }}" method="POST">

            @csrf

            @include('settings.partials.form')

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
