<div class="form-group">
    <label for="full_name">{{ __('Full name') }}</label>
    <input type="text" name="full_name" id="full_name" class="form-control" value="{{ old('full_name') ?? $botUserInfo->full_name }}" >
    @error('full_name')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="position">{{ __('Position') }}</label>
    <select name="position" id="position" class="form-control">
        @php $currentValue = old('position') ?? $botUserInfo->position; @endphp
        <option value="-">-</option>
        @foreach($positions as $value)
            <option value="{{ $value }}" @if($value == $currentValue) selected @endif>{{ $value }}</option>
        @endforeach
    </select>
    @error('position')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isActive = old('active') ?? $botUserInfo->active;
    @endphp
    <label for="active" class="mb-0">{{ __('Bot user is active') }}</label>
    <div>
        <input type="checkbox" name="active" id="active"  @if($isActive) checked @endif >
    </div>
    @error('active')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isChecked = old('dsp_manager') ?? $botUserInfo->dsp_manager;
    @endphp
    <label for="dsp_manager" class="mb-0">{{ __('DSP manager') }}</label>
    <div>
        <input type="checkbox" name="dsp_manager" id="dsp_manager"  @if($isChecked) checked @endif >
    </div>
    @error('dsp_manager')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isChecked = old('qc_manager') ?? $botUserInfo->qc_manager;
    @endphp
    <label for="qc_manager" class="mb-0">{{ __('QC manager') }}</label>
    <div>
        <input type="checkbox" name="qc_manager" id="qc_manager"  @if($isChecked) checked @endif >
    </div>
    @error('qc_manager')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isChecked = old('relations_manager') ?? $botUserInfo->relations_manager;
    @endphp
    <label for="relations_manager" class="mb-0">{{ __('Relations manager') }}</label>
    <div>
        <input type="checkbox" name="relations_manager" id="relations_manager"  @if($isChecked) checked @endif >
    </div>
    @error('relations_manager')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isChecked = old('trailers_manager') ?? $botUserInfo->trailers_manager;
    @endphp
    <label for="trailers_manager" class="mb-0">{{ __('Trailers manager') }}</label>
    <div>
        <input type="checkbox" name="trailers_manager" id="trailers_manager"  @if($isChecked) checked @endif >
    </div>
    @error('trailers_manager')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isChecked = old('trucks_manager') ?? $botUserInfo->trucks_manager;
    @endphp
    <label for="trucks_manager" class="mb-0">{{ __('Trucks manager') }}</label>
    <div>
        <input type="checkbox" name="trucks_manager" id="trucks_manager"  @if($isChecked) checked @endif >
    </div>
    @error('trucks_manager')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="shift">{{ __('Shift') }}</label>
    <select name="shift" id="shift" class="form-control selectpicker">
        @php $currentValue = old('shift') ?? $botUserInfo->shift; @endphp
        <option value="-">-</option>
        @foreach($shifts as $value)
            <option value="{{ $value }}" @if($value == $currentValue) selected @endif>{{ $value }}</option>
        @endforeach
    </select>
    @error('shift')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="team_id">{{ __('Team') }} ({{ __('DSP / DSP manager') }})</label>
    <select name="team_id" id="team_id" class="form-control selectpicker">
        @php $currentValue = old('team_id') ?? $botUserInfo->team_id; @endphp
        <option value="">-</option>
        @foreach($teams as $team)
            <option value="{{ $team->id }}" @if($team->id == $currentValue) selected @endif>{{ $team->name }}</option>
        @endforeach
    </select>
    @error('team_id')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="load_types">{{ __('Load types') }} ({{ __('QC manager') }})</label>
    <select name="load_types[]" id="load_types" class="form-control selectpicker" multiple>
        @php $currentValues = old('load_types') ?? $botUserInfo->load_types; @endphp
        @foreach(\App\Models\Load::types() as $key => $value)
            <option value="{{ $key }}" @if(is_array($currentValues) && in_array($key, $currentValues)) selected @endif>{{ $value }}</option>
        @endforeach
    </select>
    @error('load_types')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="company_codes">{{ __('Company codes') }} ({{ __('QC manager') }})</label>
    <select name="company_codes[]" id="company_codes" class="form-control selectpicker" multiple>
        @php $currentValues = old('company_codes') ?? $botUserInfo->company_codes; @endphp
        @foreach(\App\Models\Load::companyCodes() as $value)
            <option value="{{ $value }}" @if(is_array($currentValues) && in_array($value, $currentValues)) selected @endif>{{ $value }}</option>
        @endforeach
    </select>
    @error('company_codes')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

