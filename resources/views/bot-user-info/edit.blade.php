@extends('layouts.app')

@section('title', __('Edit user'))

@section('content')
    <div class="container">

        <h1>{{ __('Edit user') }}</h1>

        <div class="py-4">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('bot-user-info.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('bot-user-info.update', $botUserInfo->id) }}" method="POST" enctype="multipart/form-data">

            @csrf
            @method('PUT')

            @include('bot-user-info.partials.form')

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
