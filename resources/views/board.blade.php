@extends('layouts.app')

@section('title', __('Board'))

@section('content')
    <div class="container-fluid">

        <h1>
            {{ __('Board') }}
        </h1>

        <div class="my-4">
            <form action="{{ route('board') }}" id="main-board-filter-form">
                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_team_id">{{ __('Team') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="team_id" id="form_team_id"
                                onchange="document.getElementById('main-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($teams as $team)
                                    <option value="{{ $team->id }}" @if ((string) $team->id == $filter['team_id']) selected @endif>
                                        {{ $team->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_dispatcher_id">{{ __('Dispatcher') }}</label>
                            <select class="form-control selectpicker" data-live-search="true" name="dispatcher_id"
                                id="form_dispatcher_id"
                                onchange="document.getElementById('main-board-filter-form').submit()">
                                <option value="">{{ __('All') }}</option>
                                @foreach ($dispatchersList as $dispatcher)
                                    <option value="{{ $dispatcher->id }}" @if ((string) $dispatcher->id == $filter['dispatcher_id']) selected @endif>
                                        {{ $dispatcher->full_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        @php
            $statusesOrder = [\App\Models\DriverStatus::STATUS_READY_3, \App\Models\DriverStatus::STATUS_READY_2, \App\Models\DriverStatus::STATUS_READY_1, \App\Models\DriverStatus::STATUS_REST, \App\Models\DriverStatus::STATUS_TRANSIT, \App\Models\DriverStatus::STATUS_DISPATCHED, \App\Models\DriverStatus::STATUS_HOME, \App\Models\DriverStatus::STATUS_SHOP, \App\Models\DriverStatus::STATUS_VACATION];
        @endphp

        <div class="mb-4">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <tr>
                        @foreach ($statusesOrder as $status)
                            <td class="align-center">
                                <a href="#drivers-statuses-{{ $status }}" class="d-block"
                                    style="color: {{ \App\Models\DriverStatus::statusColors()[$status] }}">
                                    <span class="text-nowrap d-inline-flex align-items-center">
                                        {{-- <span class="mr-1">{!! \App\Models\DriverStatus::statusIcons()[$status] !!}</span> --}}
                                        <span>{{ \App\Models\DriverStatus::statuses()[$status] }}:</span>
                                    </span>
                                    <strong>{{ $drivers->where('status', $status)->count() }}</strong>
                                </a>
                            </td>
                        @endforeach
                    </tr>
                </table>
            </div>

            <div class="mb-5">
                <div class="table-responsive" style="min-height: 600px;">
                    <table class="table table-bordered table-hover">
                        <tr>
                            <th>#</th>
                            <th>{{ __('Load') }}</th>
                            <th>{{ __('Driver') }}</th>
                            <th>{{ __('Truck') }}</th>
                            <th>{{ __('Phone number') }}</th>
                            <th>{{ __('Status') }}</th>
                            <th>{{ __('Pick up') }}</th>
                            <th>{{ __('Delivery') }} / {{ __('Home address') }} / {{ __('Shop address') }}</th>
                            <th>{{ __('Trailer') }}</th>
                            <th>{{ __('PM') }}</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <select id="statuses-list" class="selectpicker w-100" data-live-search="true">
                                    <option value="-">{{ __('All') }}</option>
                                    @foreach (Helper::driverStatuses() as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select id="pu-states-list" class="selectpicker w-100" data-live-search="true">
                                    <option value="">{{ __('All') }}</option>
                                    @foreach (Helper::statesList() as $item)
                                        <option value="{{ $item }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select id="del-states-list" class="selectpicker w-100" data-live-search="true">
                                    <option value="">{{ __('All') }}</option>
                                    @foreach (Helper::statesList() as $item)
                                        <option value="{{ $item }}">{{ $item }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td></td>
                            <td></td>
                        </tr>

                        @php
                            $driverNumber = 0;
                        @endphp
                        @foreach ($statusesOrder as $status)
                            <tr id="drivers-statuses-{{ $status }}">
                                {{-- <td colspan="9" class="bg-light">
                                    {{ \App\Models\DriverStatus::statuses()[$status] }}
                                </td> --}}
                            </tr>
                            @foreach ($drivers->where('status', $status) as $driver)
                                @php
                                    $driverNumber++;
                                    $load = $driver->relatedload;
                                    $trailer = $driver->trailer;
                                @endphp
                                <tr class="driver-row" data-del-state="{{ Helper::getDelState($driver) }}"
                                    data-pu-state="{{ Helper::getPuState($driver) }}" data-status="{{ $driver->status }}">
                                    <td>
                                        {{ $driverNumber }}
                                    </td>
                                    <td>
                                        @if ($load)
                                            <a href="javascript:;" data-toggle="popover" data-trigger="focus"
                                                title="Load ID: {{ $load->load_id }}" data-html="true"
                                                data-content="@include('partials.load-popover-content')" class="text-dark">
                                                {{ $load->load_and_pcs }}
                                                <i class="fa-regular fa-eye"></i>
                                            </a>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if (auth()->user()->isAdmin())
                                            <a href="{{ route('drivers.index', ['id' => $driver->id]) }}" target="_blank"
                                                class="text-dark">
                                                {{ $driver->name_and_id }}
                                                <i class="fa-solid fa-link"></i>
                                            </a>
                                        @else
                                            {{ $driver->name }}
                                        @endif
                                    </td>
                                    <td>
                                        {{ $driver->truck_number }}
                                    </td>
                                    <td>
                                        {{ $driver->phone_number }}
                                    </td>
                                    <td style="background-color: {{ $driver->status_color }};"
                                        class="text-white text-center align-middle">
                                        <a class="text-white" href="javascript:;" data-toggle="popover" data-trigger="focus"
                                            title="Status: {{ $driver->status_title }}" data-html="true"
                                            data-content="@include('partials.driver-status-popover-content', [
                                                'driverStatus' => $driver->currentDriverStatus,
                                            ])">
                                            {{ $driver->status_title }}
                                        </a>
                                    </td>
                                    <td>
                                        @if ($load)
                                            <div class="mb-1">
                                                PU: {{ $load->pickup_location }} <br>
                                                <i>{{ $load->pickup_time }}</i>
                                            </div>
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td>
                                        @if ($load)
                                            <div>
                                                Del: {{ $load->delivery_location }} <br>
                                                <i>{{ $load->delivery_time }}</i>
                                            </div>
                                        @endif
                                        @if ($driver->currentDriverStatus && $driver->currentDriverStatus->isHome())
                                            <div>
                                                Home address: {{ $driver->home_address }}
                                            </div>
                                        @endif
                                        @if ($driver->currentDriverStatus && $driver->currentDriverStatus->shop_address)
                                            <div>
                                                Shop address: {{ $driver->currentDriverStatus->shop_address }}
                                            </div>
                                        @endif
                                    </td>
                                    <td>
                                        {{-- @if ($trailer)
                                            {{ $trailer->trailer_number }}
                                        @else
                                            -
                                        @endif --}}
                                        <div id="drivers-{{ $driver->id }}-trailer-info" data-table="drivers" data-field="trailer_info" data-id="{{ $driver->id }}">
                                            {{ $driver->trailer_info }}
                                        </div>
                                        <a href="javascript:;" class="edit-table-info-btn" data-target="#drivers-{{ $driver->id }}-trailer-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                    <td>
                                        <div id="drivers-{{ $driver->id }}-pm-info" data-field="pm_info" data-table="drivers" data-field="pm_info" data-id="{{ $driver->id }}">
                                            {{ $driver->pm_info }}
                                        </div>
                                        <a href="javascript:;" class="edit-table-info-btn" data-target="#drivers-{{ $driver->id }}-pm-info">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endforeach

                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('#del-states-list').on('change', filterDrivers)
        $('#pu-states-list').on('change', filterDrivers)
        $('#statuses-list').on('change', filterDrivers)
        filterDrivers()

        function filterDrivers() {
            $('.driver-row').show();
            let delState = $('#del-states-list').val()
            let puState = $('#pu-states-list').val()
            let status = $('#statuses-list').val()
            $('.driver-row').each(function() {
                if (delState && $(this).data('del-state') != delState) {
                    $(this).hide()
                }
                if (puState && $(this).data('pu-state') != puState) {
                    $(this).hide()
                }
                if (status != '-' && $(this).data('status') != status) {
                    $(this).hide()
                }
            })
        }
    </script>
@endsection
