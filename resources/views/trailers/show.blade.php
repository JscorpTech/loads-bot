@extends('layouts.app')

@section('title', __('Team') . '#' . $team)

@section('content')
    <div class="container">

    	<h1>
    		{{ __('Team') }} #{{ $team }}
    	</h1>

        <div class="py-4 d-print-none">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('teams.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

    </div>
@endsection
