@extends('layouts.app')

@section('title', __('Trailers'))

@section('content')
    <div class="container">

        @include('partials.alerts')

        <div class="my-4">
            <a href="{{ route('trailers.create') }}" class="btn btn-lg btn-info">
                {{ __('Add trailer') }}
            </a>
        </div>


        <h1>{{ __('Trailers') }}</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-light table-hover">
                <tr class="table-active">
                    <th>
                        {{ __('Title') }}
                    </th>
                    <th></th>
                </tr>
                @forelse($trailers as $trailer)
                    <tr>
                        <td>{{ $trailer->trailer_number }}</td>
                        <td class="text-nowrap">
                            <a href="{{ route('trailers.edit', ['trailer' => $trailer->id]) }}" class="btn btn-sm btn-warning">{{ __('Edit') }}</a>
                            <a href="#" onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-trailer-{{ $trailer->id }}').submit() }" class="btn btn-sm btn-danger">{{ __('Delete') }}</a>
                            <form action="{{ route('trailers.destroy', ['trailer' => $trailer->id]) }}" method="post" id="delete-trailer-{{ $trailer->id }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                    </tr>
                @endforelse
            </table>
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
