@extends('layouts.app')

@section('title', __('Teams'))

@section('content')
    <div class="container">

        @include('partials.alerts')

        <div class="my-4">
            <a href="{{ route('teams.create') }}" class="btn btn-lg btn-info">
                {{ __('Add team') }}
            </a>
        </div>


        <h1>{{ __('Teams') }}</h1>
        <div class="table-responsive">
            <table class="table table-bordered table-light table-hover">
                <tr class="table-active">
                    <th>
                        {{ __('Title') }}
                    </th>
                    <th></th>
                </tr>
                @forelse($teams as $team)
                    <tr>
                        <td>{{ $team->name }}</td>
                        <td class="text-nowrap">
                            <a href="{{ route('teams.edit', ['team' => $team->id]) }}" class="btn btn-sm btn-warning">{{ __('Edit') }}</a>
                            <a href="#" onclick="event.preventDefault(); if (confirm('{{ __('Are you sure?') }}')) { $('#delete-team-{{ $team->id }}').submit() }" class="btn btn-sm btn-danger">{{ __('Delete') }}</a>
                            <form action="{{ route('teams.destroy', ['team' => $team->id]) }}" method="post" id="delete-team-{{ $team->id }}">
                                @csrf
                                @method('DELETE')
                            </form>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="8" class="text-center">{{ __('Nothing found') }}</td>
                    </tr>
                @endforelse
            </table>
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
