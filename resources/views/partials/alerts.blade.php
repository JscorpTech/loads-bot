@if (session()->has('success'))
    <div class="alert alert-success">
        {{ __(session()->get('success')) }}
    </div>
@endif

@if (session()->has('error'))
    <div class="alert alert-danger">
        {{ __(session()->get('error')) }}
    </div>
@endif

@if (session()->has('warning'))
    <div class="alert alert-warning">
        {{ __(session()->get('warning')) }}
    </div>
@endif
