@php
    $weekMinutes = 10080; // 7 * 24 * 60
@endphp
<div class="mb-5">
    <table class="table table-borderless table-hover">
        <tr class="position-sticky top-0" style="z-index: 100">
            <th class="text-white bg-primary" style="width: 500px">
                <h4 class="mb-0">
                    <strong>{{ $dispatcher->full_name }}</strong>
                </h4>
            </th>
            @for ($i = 0; $i < 7; $i++)
                @php
                    $day = (clone $weekStart)->addDays($i);
                @endphp
                <th class="text-dark bg-warning">
                    <strong>{{ Helper::formatDate($day) }}</strong>
                    @if ($day == now()->startOfDay())
                        ({{ __('Today') }})
                    @endif
                </th>
            @endfor
            <th class="bg-white">{{ __('Manager comment') }}</th>
            <th class="bg-white">{{ __('DSP comment') }}</th>
        </tr>
        @foreach ($dispatcher->botUser->drivers as $driver)
            @php
                $driverPlan = $driver->driverPlans->sortByDesc('id')->first();
                $weeklyStats = Helper::getDriverWeeklyStats($driver, $weekStart, $weekEnd);
                $dailyStats = [];
                for ($i = 0; $i < 7; $i++) {
                    $dailyStats[$i] = Helper::getDriverDailyStats($driver, (clone $weekStart)->addDays($i), $weeklyStats['loads']);
                }
            @endphp
            <tr>
                <td class="py-2">
                    <div class="mb-1">
                        @if (auth()->user()->isAdmin())
                            <a href="{{ route('drivers.index', ['id' => $driver->id]) }}" target="_blank"
                                class="text-dark">
                                {{ $driver->name_and_id }}
                                <i class="fa-solid fa-link"></i>
                            </a>
                        @else
                            {{ $driver->name }}
                        @endif
                    </div>
                    <div class="mb-1">
                        {{ $driver->type_title }}
                    </div>
                    <div>
                        {{ __('Status') }}:
                        <a href="javascript:;" data-toggle="popover" data-trigger="focus"
                            title="Status: {{ $driver->status_title }}" data-html="true"
                            data-content="@include('partials.driver-status-popover-content', [
                                'driverStatus' => $driver->currentDriverStatus,
                            ])">
                            {{ $driver->status_title }}
                        </a>
                    </div>
                </td>
                @for ($i = 0; $i < 7; $i++)
                    <td class="py-2">
                        <div class="mb-1">
                            <strong>{{ __('Total $') }}</strong>:
                            {{ Helper::formatPrice($dailyStats[$i]['loadsTotal']) }}
                        </div>
                        <div class="mb-1">
                            <strong>{{ __('Total mile') }}</strong>:
                            {{ $dailyStats[$i]['loadsMile'] }}
                        </div>
                        <div>
                            <strong>{{ __('Per mile') }}</strong>:
                            {{ Helper::formatPrice($dailyStats[$i]['perMile']) }}
                        </div>
                    </td>
                @endfor
                <td>
                    @if ($driverPlan)
                        <div id="driver-plans-{{ $driverPlan->id }}-manager-comment" data-table="driver_plans"
                            data-field="manager_comment" data-id="{{ $driverPlan->id }}">
                            {{ $driverPlan->manager_comment }}
                        </div>
                        @can('update-kpi-board-manager-comment')
                            <a href="javascript:;" class="edit-table-info-btn"
                                data-target="#driver-plans-{{ $driverPlan->id }}-manager-comment">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endcan
                    @endif
                </td>
                <td>
                    @if ($driverPlan)
                        <div id="driver-plans-{{ $driverPlan->id }}-dsp-comment" data-table="driver_plans"
                            data-field="dsp_comment" data-id="{{ $driverPlan->id }}">
                            {{ $driverPlan->dsp_comment }}
                        </div>
                        @can('update-kpi-board-dsp-comment')
                            <a href="javascript:;" class="edit-table-info-btn"
                                data-target="#driver-plans-{{ $driverPlan->id }}-dsp-comment">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endcan
                    @endif
                </td>
            </tr>
            <tr>
                <td class="py-2">
                    <div class="py-1 px-2 bg-warning">
                        <strong>{{ __('Weekly target') }}</strong>:
                        <span class="text-nowrap">{{ Helper::formatPrice($weeklyStats['weeklyTarget']) }}</span> /
                        <span class="text-nowrap">{{ $weeklyStats['weeklyTargetMile'] }} mile</span>
                    </div>
                    <div
                        class="py-1 px-2 text-{{ $weeklyStats['completionStatus'] }} @if (in_array($weeklyStats['completionStatus'], ['warning'])) bg-dark @endif">
                        <strong>{{ __('Total $') }}</strong>:
                        {{ Helper::formatPrice($weeklyStats['loadsTotal']) }}
                        {{-- @if ($user->isAdmin())
                            | <strong>{{ $weeklyStats['completedPercent'] }}%</strong>
                        @endif --}}

                    </div>
                    <div class="py-1 px-2">
                        <strong>{{ __('Total mile') }}</strong>:
                        {{ $weeklyStats['loadsMile'] }}
                    </div>
                    <div class="py-1 px-2">
                        <strong>{{ __('Authorized loaded mile') }}</strong>:
                        {{ $weeklyStats['loadsAuthorizedLoadedMile'] }}
                    </div>
                    <div class="py-1 px-2">
                        <strong>{{ __('Load DH mile') }}</strong>:
                        {{ $weeklyStats['loadsDeadheadMile'] }}
                    </div>
                    <div class="py-1 px-2">
                        <strong>{{ __('Authorized mile (PLD, TRL, FLT miles)') }}</strong>:
                        {{ $weeklyStats['loadsAuthorizedMile'] }}
                    </div>
                    <div class="py-1 px-2">
                        <strong>{{ __('Unauthorized miles (including Home DH miles)') }}</strong>:
                        {{ $weeklyStats['loadsUnauthorizedMile'] }}
                    </div>
                    <div
                        class="py-1 px-2 text-{{ $weeklyStats['perMileStatus'] }} @if (in_array($weeklyStats['perMileStatus'], ['warning'])) bg-dark @endif">
                        <strong>{{ __('Per mile') }}</strong>:
                        {{ Helper::formatPrice($weeklyStats['perMile']) }}
                    </div>
                    @if (auth()->user()->isAdmin())
                        <div class="py-1 px-2 text-{{ $weeklyStats['profit'] > 0 ? 'success' : 'danger' }}">
                            <strong>{{ __('Profit') }}</strong>:
                            {{ Helper::formatPrice($weeklyStats['profit']) }}
                        </div>
                    @endif
                </td>
                @for ($i = 0; $i < 7; $i++)
                    <td class="py-2">
                        @if ($dailyStats[$i]['loads']->isNotEmpty())
                            <div>
                                <div>Loads:</div>
                                @foreach ($dailyStats[$i]['loads'] as $load)
                                    <div>
                                        <a href="javascript:;" data-toggle="popover" data-trigger="focus"
                                            title="Load ID: {{ $load->load_id }}" data-html="true"
                                            data-content="@include('partials.load-popover-content')" class="text-dark">

                                            <strong
                                                class="@if ($load->isCancelled()) text-decoration-line-through @endif">{{ $load->load_id }}</strong>
                                            @if ($load->edited_times > 0)
                                                <span class="text-danger">Edited times:
                                                    {{ $load->edited_times }}</span> <br>
                                            @endif
                                            <strong>{{ __('PU address') }}</strong>: {{ $load->pickup_location }} <br>
                                            <strong> {{ __('Del address') }}</strong>:
                                            {{ $load->delivery_location }} <br>
                                            <i class="fa-regular fa-eye"></i>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </td>
                @endfor
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <h5>{{ __('Timeline') }}</h5>
                    <div style="width: 100%; height: 300px">
                        <canvas class="driver-statuses-pie-chart" data-id="{{ $driver->id }}"></canvas>
                    </div>
                </td>
                <td colspan="7" class="px-0 pt-5 position-relative">
                    <div class="position-absolute d-flex w-100 h-100 left-0 top-0 py-1" style="z-index: -100">
                        @for ($i = 0; $i < 7; $i++)
                            @php
                                $day = (clone $weekStart)->addDays($i);
                            @endphp
                            <div class="flex-fill @if($i == 0) border-left @endif border-right h-100 d-flex align-items-start justify-content-center">
                                <strong class="text-black-50">{{ $day->format('M d') }}</strong>
                            </div>
                        @endfor
                    </div>
                    <div class="position-relative" style="z-index: 10">
                        @foreach ($driver->driverStatuses as $driverStatus)
                            @php
                                $statusStart = $driverStatus->start_time;
                                if ($statusStart < $weekStart) {
                                    $statusStart = clone $weekStart;
                                }
                                $statusEnd = $driverStatus->end_time;
                                if ($statusEnd > $weekEnd || empty($statusEnd)) {
                                    $statusEnd = clone $weekEnd;
                                }
                                $statusLength = $statusStart->diffInMinutes($statusEnd);
                                if ($statusLength < 10) {
                                    continue;
                                }
                                $emptyBlockPercent = round(($weekStart->diffInMinutes($statusStart) / $weekMinutes) * 100, 2);
                                $filledBlockPercent = round(($statusLength / $weekMinutes) * 100, 2);
                            @endphp
                            <input type="hidden" class="driver-status-input" data-driver-id="{{ $driver->id }}" data-status="{{ $driverStatus->status_title }}" data-length="{{ $statusLength }}" data-color="{{ $driverStatus->status_color }}">
                            <div class="d-flex">
                                <div style="flex: 0 0 {{ $emptyBlockPercent }}%"></div>
                                <div data-toggle="tooltip" data-placement="top" data-html="true"
                                    title="{{ $driverStatus->status_title }} <br> {{ $driverStatus->start_time->format('M d, Y H:i') }} <br> {{ $driverStatus->end_time ? $driverStatus->end_time->format('M d, Y H:i') : '' }}"
                                    style="flex: 0 0 {{ $filledBlockPercent }}%; height: 14px; background-color: {{ $driverStatus->status_color }};color: #fff; white-space: nowrap; overflow: hidden; line-height: 1; font-size: 10px; padding: 2px; text-align: center;">
                                    {{ $driverStatus->status_title }}</div>
                            </div>
                        @endforeach
                    </div>
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td class="p-1 bg-dark"></td>
                <td class="p-1 bg-gray" colspan="9"></td>
            </tr>
        @endforeach
    </table>
</div>
