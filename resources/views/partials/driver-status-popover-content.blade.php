@if ($driverStatus)
    <div><strong>{{ __('Status') }}:</strong> {{ $driverStatus->status_title }}</div>
    <div><strong>{{ __('Start') }}:</strong> {{ Helper::formatDateTime($driverStatus->start_time) }}</div>
    <div><strong>{{ __('End') }}:</strong> {{ $driverStatus->end_time ? Helper::formatDateTime($driverStatus->end_time) : '-' }}</div>
    <div><strong>{{ __('Hashtag') }}:</strong> {{ $driverStatus->hashtag }}</div>
    <div><strong>{{ __('Comment') }}:</strong> {{ $driverStatus->comment }}</div>
@endif
