<footer id="sticky-footer" class="py-4 bg-dark text-white-50 d-print-none">
    <div class="container-fluid text-center">
        <small>&copy; {{ config('app.name') }} {{ date('Y') }}</small>
    </div>
</footer>
