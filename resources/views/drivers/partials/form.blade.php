<div class="form-group">
    <label for="form_name">{{ __('Name') }}</label>
    <input type="text" name="name" id="form_name" class="form-control" value="{{ old('name') ?? $driver->name }}" required>
    @error('name')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="type">{{ __('Type') }}</label>
    <select name="type" id="type" class="form-control selectpicker">
        @php
            $currentValue = old('type') ?? $driver->type;
        @endphp
        <option value="">-</option>
        @foreach(\App\Models\Driver::types() as $key => $value)
            <option value="{{ $key }}" @if($key == $currentValue) selected @endif>{{ $key }} - {{ $value }}</option>
        @endforeach
    </select>
    @error('type')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_phone_number">{{ __('Phone number') }}</label>
    <input type="text" name="phone_number" id="form_phone_number" class="form-control" value="{{ old('phone_number') ?? $driver->phone_number }}" required>
    @error('phone_number')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_weekly_target">{{ __('Weekly target rate') }}</label>
    <input type="text" name="weekly_target" id="form_weekly_target" class="form-control" value="{{ old('weekly_target') ?? $driver->weekly_target }}" required>
    @error('weekly_target')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_weekly_target_mile">{{ __('Weekly target mile') }}</label>
    <input type="text" name="weekly_target_mile" id="form_weekly_target_mile" class="form-control" value="{{ old('weekly_target_mile') ?? $driver->weekly_target_mile }}" required>
    @error('weekly_target_mile')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_pay_per_mile">{{ __('Pay per mile') }}</label>
    <input type="text" name="pay_per_mile" id="form_pay_per_mile" class="form-control" value="{{ old('pay_per_mile') ?? $driver->pay_per_mile }}" required>
    @error('pay_per_mile')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_penske_pay_per_mile">{{ __('Penske pay per mile') }}</label>
    <input type="text" name="penske_pay_per_mile" id="form_penske_pay_per_mile" class="form-control" value="{{ old('penske_pay_per_mile') ?? $driver->penske_pay_per_mile }}" required>
    @error('penske_pay_per_mile')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_fixed_cost">{{ __('Fixed cost') }}</label>
    <input type="text" name="fixed_cost" id="form_fixed_cost" class="form-control" value="{{ old('fixed_cost') ?? $driver->fixed_cost }}" required>
    @error('fixed_cost')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_truck_number">{{ __('Truck number') }}</label>
    <input type="text" name="truck_number" id="form_truck_number" class="form-control" value="{{ old('truck_number') ?? $driver->truck_number }}" required>
    @error('truck_number')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="dispatcher_id">{{ __('Main dispatcher') }}</label>
    <select name="dispatcher_id" id="dispatcher_id" class="form-control selectpicker">
        @php
            $currentValue = old('dispatcher_id') ?? $driver->dispatcher_id;
        @endphp
        <option value="">-</option>
        @foreach($dispatchers as $dispatcher)
            <option value="{{ $dispatcher->id }}" @if($dispatcher->id == $currentValue) selected @endif>{{ $dispatcher->botUserInfo->full_name }}</option>
        @endforeach
    </select>
    @error('dispatcher_id')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="dispatcher_ids">{{ __('Additional dispatchers') }}</label>
    <select name="dispatcher_ids[]" id="dispatcher_ids" class="form-control selectpicker" multiple data-live-search="true">
        @php
            $currentValues = old('dispatcher_ids') ?? $driver->additionalDispatchers->pluck('id')->toArray();
        @endphp
        <option value="">-</option>
        @foreach($dispatchers as $dispatcher)
            <option value="{{ $dispatcher->id }}" @if(in_array($dispatcher->id, $currentValues)) selected @endif>{{ $dispatcher->botUserInfo->full_name }}</option>
        @endforeach
    </select>
    @error('dispatcher_ids')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_home_address">{{ __('Home address (Example: ' . Helper::exampleAddress() . ')') }}</label>
    <input type="text" name="home_address" id="form_home_address" class="form-control" value="{{ old('home_address') ?? $driver->home_address }}" required>
    @error('home_address')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    <label for="form_telegram_username">{{ __('Telegram username') }}</label>
    <input type="text" name="telegram_username" id="form_telegram_username" class="form-control" value="{{ old('telegram_username') ?? $driver->telegram_username }}" required>
    @error('telegram_username')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>

<div class="form-group">
    @php
        $isActive = old('active') ?? $driver->active;
    @endphp
    <label for="active" class="mb-0">{{ __('Driver is active') }}</label>
    <div>
        <input type="checkbox" name="active" id="active"  @if($isActive) checked @endif >
    </div>
    @error('active')
    <div class="invalid-feedback d-block">{{ $message }}</div>
    @enderror
</div>
