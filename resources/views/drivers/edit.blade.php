@extends('layouts.app')

@section('title', __('Edit driver'))

@section('content')
    <div class="container">

        <h1>{{ __('Edit driver') }}</h1>

        <div class="py-4">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('drivers.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <form action="{{ route('drivers.update', $driver->id) }}" method="POST">

            @csrf
            @method('PUT')

            @include('drivers.partials.form')

            <div class="form-group">
                <button class="btn btn-lg btn-success" type="submit">{{ __('Save') }}</button>
            </div>
        </form>

    </div>
@endsection
