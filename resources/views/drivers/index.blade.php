@extends('layouts.app')

@section('title', __('Drivers'))

@section('content')
    <div class="container-fluid">

        @include('partials.alerts')

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-light table-bordered table-hover">
                    <tr class="table-active">
                        <th>{{ __('Drivers') }}</th>
                    </tr>
                    <tr>
                        <td>
                            <strong>All: {{ $drivers->total() }}</strong> <br>
                            Active: {{ $activeDriversQty }}
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="my-4">
            <a href="{{ route('drivers.create') }}" class="btn btn-lg btn-info">
                {{ __('Add driver') }}
            </a>
            <a href="{{ route('stats.drivers') }}" class="btn btn-lg btn-primary">
                {{ __('Stats') }}
            </a>
        </div>

        <h1>{{ __('Drivers') }}</h1>

        <div class="my-4">
            <form action="{{ route('drivers.index') }}" id="drivers-filter-form">

                <div class="row">
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_name">{{ __('Name') }}</label>
                                <input type="text" id="form_name" name="name" class="form-control"
                                    value="{{ $filter['name'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <div class="form-group">
                                <label for="form_truck_number">{{ __('Truck number') }}</label>
                                <input type="text" id="form_truck_number" name="truck_number" class="form-control"
                                    value="{{ $filter['truck_number'] }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-6 col-md-3">
                        <div class="form-group">
                            <label for="form_status">{{ __('Status') }}</label>
                            <select class="form-control" name="status" id="form_status">
                                <option value="-">-</option>
                                @foreach ($statuses as $key => $value)
                                    <option value="{{ $key }}" @if ((string) $key == $filter['status']) selected @endif>
                                        {{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <button class="btn btn-primary" type="submit">{{ __('Apply filter') }}</button>
                </div>
            </form>
        </div>

        <div class="my-4">
            <div class="table-responsive">
                <table class="table table-bordered table-light table-hover">
                    <tr class="table-active">
                        <th>
                            {{ __('ID') }}
                        </th>
                        <th>
                            {{ __('Name') }}
                        </th>
                        <th>
                            {{ __('Phone number') }}
                        </th>
                        <th>
                            {{ __('Chat ID') }}
                        </th>
                        <th>
                            {{ __('Truck number') }}
                        </th>
                        <th>
                            {{ __('Payment') }}
                        </th>
                        <th>
                            {{ __('Created time') }}
                        </th>
                        <th>
                            {{ __('Weekly target rate') }}
                        </th>
                        <th>
                            {{ __('Weekly target mile') }}
                        </th>
                        <th>
                            {{ __('Main dispatcher') }}
                        </th>
                        <th>
                            {{ __('Additional dispatchers') }}
                        </th>
                        <th>
                            {{ __('Status') }}
                        </th>
                        <th>
                            {{ __('Active') }}
                        </th>
                        <th>
                            {{ __('Home address') }}
                        </th>
                        <th></th>
                    </tr>
                    @forelse($drivers as $driver)
                        <tr>
                            <td>{{ $driver->id }}</td>
                            <td>
                                {{ $driver->name }} <br>
                                {{ $driver->type_title }}
                            </td>
                            <td>{{ $driver->phone_number }}</td>
                            <td>
                                <div>{{ $driver->chat_id ?? '-' }}</div>
                                <div>
                                    <span id="chat-bind-code-{{ $driver->id }}">{{ $driver->chat_bind_code }}</span>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-sm btn-primary copy-btn" data-content="Copied"
                                        data-target="#chat-bind-code-{{ $driver->id }}"><i
                                            class="fa-regular fa-copy"></i> Copy code</button>
                                </div>
                            </td>
                            <td>{{ $driver->truck_number }}</td>
                            <td>
                                <div>
                                    {{ __('Pay per mile') }}: {{ $driver->pay_per_mile }}
                                </div>
                                <div>
                                    {{ __('Penske pay per mile') }}: {{ $driver->penske_pay_per_mile }}
                                </div>
                                <div>
                                    {{ __('Fixed cost') }}: {{ $driver->fixed_cost }}
                                </div>
                            </td>
                            <td>{{ Helper::formatDateTime($driver->created_at) }}</td>
                            <td>{{ $driver->weekly_target }}</td>
                            <td>{{ $driver->weekly_target_mile }}</td>
                            <td>{{ $driver->dispatcher_name }}</td>
                            <td>{{ $driver->additional_dispatchers_names }}</td>
                            <td>{{ $driver->status_title }}</td>
                            <td>{{ $driver->active ? __('Yes') : __('No') }}</td>
                            <td>{{ $driver->home_address }}</td>
                            <td class="text-nowrap">
                                <a href="{{ route('drivers.edit', ['driver' => $driver->id]) }}"
                                    class="btn btn-sm btn-warning">{{ __('Edit') }}</a>
                                <a href="{{ route('drivers.stats', ['driver' => $driver->id]) }}"
                                    class="btn btn-sm btn-primary">{{ __('Stats') }}</a>
                                <a href="{{ route('driver-statuses.index', ['driver_id' => $driver->id]) }}"
                                    class="btn btn-sm btn-info">{{ __('Status') }}</a>
                                <a href="{{ route('drivers.history', ['driver' => $driver->id]) }}"
                                    class="btn btn-sm btn-info">{{ __('History') }}</a>
                                @if (!$driver->isActive())
                                    <a href="javascript:;"
                                        onclick="confirm('{{ __('Are you sure?') }}') && $('#delete-driver-{{ $driver->id }}').submit()"
                                        class="btn btn-sm btn-danger">{{ __('Delete') }}</a>
                                    <form action="{{ route('drivers.destroy', ['driver' => $driver->id]) }}" method="post"
                                        id="delete-driver-{{ $driver->id }}">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                @endif

                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="15" class="text-center">{{ __('Nothing found') }}</td>
                        </tr>
                    @endforelse
                </table>
            </div>
            {{ $drivers->withQueryString()->links() }}
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
