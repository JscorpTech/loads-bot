@extends('layouts.app')

@section('title', __('Driver') . ' ' . $driver->name . ' ' . __('History'))

@section('content')
    <div class="container">

    	<h1>
    		{{ __('Driver') }} {{ $driver->name }} {{ __('History') }}
    	</h1>

        <div class="py-4 d-print-none">
            <a href="{{ (url()->previous() && url()->previous() != url()->current()) ? url()->previous() : route('drivers.index') }}" class="btn btn-info">{{ __('Back') }}</a>
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-light table-hover">
                <tr class="table-active">
                    <th>
                        {{ __('Date') }}
                    </th>
                    <th>
                        {{ __('Message') }}
                    </th>
                </tr>
                @forelse($changelogs as $changelog)
                    <tr>
                        <td>{{ Helper::formatDateTime($changelog->created_at) }}</td>
                        <td>{{ $changelog->message }}</td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="2" class="text-center">{{ __('Nothing found') }}</td>
                    </tr>
                @endforelse
            </table>
        </div>


        <br>
        <br>
        <br>
        <br>
        <br>

    </div>
@endsection
