<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Models\BotUserInfo;
use App\Models\Load;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\Update;
use Throwable;

class LoadshowCommand extends UserCommand
{
    protected $name = 'loadshow';
    protected $description = 'Load show command';
    protected $usage = '/loadshow';
    protected $version = '1.0.0';
    protected $private_only = true;
    protected $conversation;

    private $callbackdata;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = (string)$message->getMessageId();
        $user = $message->getFrom();
        $user_id = (string)$user->getId();
        $chat = $message->getChat();
        $chat_id = (string)$chat->getId();
        $text = trim($message->getText(true));

        // Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];
        $result = Request::emptyResponse();

        $userInfo = BotHelper::getUserInfo($user_id);

        // parse data
        $this->callbackdata = BotHelper::parseCallbackData($text);
        if (isset($this->callbackdata['loadshow'])) {
            $id = (int)$this->callbackdata['loadshow'];
            $load = Load::find($id);

            if ($load) {
                // send file photo/pdf
                if ($load->file_type == Load::FILE_TYPE_PHOTO) {
                    Request::sendPhoto([
                        'chat_id' => $chat_id,
                        'photo' => $load->file_id,
                    ]);
                } elseif ($load->file_type == Load::FILE_TYPE_DOCUMENT) {
                    Request::sendDocument([
                        'chat_id' => $chat_id,
                        'document' => $load->file_id,
                    ]);
                }

                // send info
                $data['text'] = $load->generateInfoTelegram();
                if ($userInfo->isQc()) {
                    $buttons = [
                        [
                            [
                                'text' => 'Update', 'callback_data' => 'loadupdate:' . $load->id,
                            ]
                        ]
                    ];
                    $data['reply_markup'] = new InlineKeyboard(...$buttons);
                } elseif ($userInfo->isDsp()) {
                    $buttons = [
                        [
                            [
                                'text' => 'Edit', 'callback_data' => 'loadedit:' . $load->id,
                            ]
                        ]
                    ];
                    $data['reply_markup'] = new InlineKeyboard(...$buttons);
                }
                $result = Request::sendMessage($data);
            } else {
                // error
                $result = Request::emptyResponse();
            }
        } else {
            // error
            $result = Request::emptyResponse();
        }

        return $result;
    }
}
