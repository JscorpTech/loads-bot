<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\DriverStatus;
use App\Models\Load;
use App\Models\State;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\Update;
use Throwable;

class LoadcreateCommand extends UserCommand
{
    protected $name = 'loadcreate';
    protected $description = 'Load create command';
    protected $usage = '/loadcreate';
    protected $version = '1.0.0';
    protected $private_only = true;
    protected $conversation;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = $message->getMessageId();
        $user = $message->getFrom();
        $user_id = (string)$user->getId();
        $chat = $message->getChat();
        $chat_id = (string)$chat->getId();
        $text = trim($message->getText(true));

        $userInfo = BotHelper::getUserInfo($user_id);

        // check user active
        if (!$userInfo || !$userInfo->isActive()) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Your account has not been activated yet'),
                'reply_markup' => StartCommand::getKeyboard($user_id),
            ]);
        }

        // check user filled
        $update = json_decode($this->update->toJson(), true);
        if (!$userInfo || !$userInfo->isFilled()) {
            $update['message']['text'] = '/start';
            return (new StartCommand($this->telegram, new Update($update)))->preExecute();
        }

        // Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];
        $result = Request::emptyResponse();

        // only DSP can create
        if (!$userInfo->isDsp()) {
            $data['text'] = 'You are not DSP';
            $data['reply_markup'] = StartCommand::getKeyboard($user_id);
            return Request::sendMessage($data);
        }

        // check other conversations
        $conversation = new Conversation($user_id, $chat_id);
        if ($conversation->exists() && $conversation->getCommand() != $this->getName()) {
            $conversation->notes = [];
            $conversation->update();
            $conversation->stop();
        }

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        // back
        if ($text == BotHelper::t('Button Back')) {
            $state--;
            $text = '';
        } elseif ($text == BotHelper::t('Button Cancel')) {
            $state = -1;
        }

        // cancel request
        if ($state == -1) {
            $notes = [];
            $this->conversation->update();
            $this->conversation->stop();
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Welcome'),
                'reply_markup' => StartCommand::getKeyboard($user_id),
            ]);
        }

        switch ($state) {

            case 0:
                if ($text === '' || !in_array($text, array_values(Load::types()))) {
                    $notes['state'] = 0;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('Type');
                    $buttons = array_chunk(array_values(Load::types()), 2);
                    $buttons[] = [BotHelper::t('Button Back')];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                $type = array_search($text, Load::types());
                $notes['type'] = $type === false ? Load::TYPE_MARKET_SPOT : $type;

                $isThirdParty = in_array($notes['type'], Load::thirdPartyTypes());
                $notes['isThirdParty'] = $isThirdParty ? true : false;

                $text = '';

                // no break
            case 1:
                if ($text === '') {
                    $notes['state'] = 1;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('Send Load Info') . "\n";
                    Request::sendMessage($data);

                    $data['text'] = '';
                    foreach (BotHelper::loadRows($notes['isThirdParty']) as $key => $value) {
                        $data['text'] .= $value[1] . ': ' . "\n";
                    }

                    $buttons = [
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                // parse and validate
                $userRowsParsed = [];
                $userRows = explode("\n", $text);
                $loadRows = BotHelper::loadRows($notes['isThirdParty']);
                foreach ($loadRows as $key => $value) {
                    if (empty($userRows[$key]) || !Str::startsWith($userRows[$key], $value[1])) {
                        // send invalid text message
                        $data['text'] = 'All fields are required';
                        return Request::sendMessage($data);
                    }
                    $userRowsParsed[$value[0]] = trim(Str::replaceFirst($value[1] . ':', '', $userRows[$key]));
                }

                $validationFields = [
                    'company_code' => 'required|in:"' . implode('","', Load::companyCodes()) . '"',
                    'load_id' => 'required',
                    'pcs_number' => 'required|digits:6|unique:loads,pcs_number',
                    'reference_no' => 'nullable|max:255',
                    'rate' => 'required|numeric|min:0|max:20000',
                    'authorized_loaded_mile' => 'required|numeric|min:0',
                    'deadhead_mile' => 'required|numeric|min:0',
                    'authorized_mile' => 'required|numeric|min:0',
                    'unauthorized_mile' => 'required|numeric|min:0',
                    'pickup_location' => 'required|regex:' . Helper::addressRegex(),
                    // 'pickup_timezone' => 'required|in:"' . implode('","', Load::timezones()) . '"',
                    'local_pickup_time' => 'required|date_format:Y-m-d H:i',
                    'pickup_time_for_driver' => 'required|date_format:Y-m-d H:i',
                    'delivery_location' => 'required|regex:' . Helper::addressRegex(),
                    // 'delivery_timezone' => 'required|in:"' . implode('","', Load::timezones()) . '"',
                    'local_delivery_time' => 'required|date_format:Y-m-d H:i|after:local_pickup_time',
                    'delivery_time_for_driver' => 'required|date_format:Y-m-d H:i|after:pickup_time_for_driver',
                    'special_note' => 'required',
                    'check_in_as' => 'nullable|max:65000',
                    'trailer' => 'nullable|max:65000',
                    'trailer_note' => 'nullable|max:65000',
                    'pickup_additional_stops' => 'nullable|max:65000',
                    'warnings' => 'nullable|max:65000',
                ];
                if ($notes['isThirdParty']) {
                    $validationFields['third_company'] = 'required';
                } else {
                    $validationFields['driver'] = [
                        'required',
                        function ($attribute, $value, $fail) {
                            if (Driver::active()->where('name', $value)->count() == 0) {
                                $fail('Driver not found');
                            }
                        },
                    ];
                }
                $validator = Validator::make($userRowsParsed, $validationFields);

                if ($validator->fails()) {
                    $data['text'] = 'Invalid data';
                    foreach ($validator->errors()->all() as $value) {
                        $data['text'] .= "\n" . $value;
                    }
                    return Request::sendMessage($data);
                }

                // additional validations
                $states = State::all();
                $additionalErrors = [];
                $puAddressParsed = Helper::parseAddress($userRowsParsed['pickup_location']);
                $puState = $states->where('code', $puAddressParsed['state'])->first();
                $delAddressParsed = Helper::parseAddress($userRowsParsed['delivery_location']);
                $delState = $states->where('code', trim($delAddressParsed['state']))->first();
                if (!$puState) {
                    $additionalErrors[] = 'Invalid pickup address state code';
                }
                if (!$delState) {
                    $additionalErrors[] = 'Invalid delivery address state code';
                }
                if (count($additionalErrors)) {
                    $data['text'] = 'Invalid data';
                    foreach ($additionalErrors as $additionalError) {
                        $data['text'] .= "\n" . $additionalError;
                    }
                    return Request::sendMessage($data);
                }

                // valid, write to notes
                foreach ($userRowsParsed as $key => $value) {
                    $notes[$key] = $value;
                }
                $notes['pickup_timezone'] = $puState->timezone_code;
                $notes['delivery_timezone'] = $delState->timezone_code;

                $puDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $notes['local_pickup_time']), $notes['pickup_timezone']);
                $delDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $notes['local_delivery_time']), $notes['delivery_timezone']);

                // check status
                if (!$notes['isThirdParty']) {
                    $driver = Driver::active()->where('name', $notes['driver'])->first();
                    if ($driver) {
                        $driverStatuses = DriverStatus::query()
                            ->where('driver_id', $driver->id)
                            ->where(function ($q1) use ($puDate, $delDate) {
                                $q1
                                    ->where(function ($q2) use ($puDate) {
                                        $q2
                                            ->where('start_time', '<=', $puDate)
                                            ->where(function ($q3) use ($puDate) {
                                                $q3
                                                    ->where('end_time', '>=', $puDate)
                                                    ->orWhereNull('end_time');
                                            });
                                    })
                                    ->orWhere(function ($q2) use ($puDate, $delDate) {
                                        $q2
                                            ->where('start_time', '>=', $puDate)
                                            ->where('start_time', '<=', $delDate);
                                    });
                            })
                            ->get();

                        foreach ($driverStatuses as $driverStatus) {
                            if (in_array($driverStatus->status, DriverStatus::loadCreateUnavailableStatuses())) {
                                $data['text'] = 'Can not create load - PU time ' . $notes['local_pickup_time'] . '. Status is ' . $driverStatus->status_title . '. Status ID: ' . $driverStatus->id . '. Start time: ' . $driverStatus->start_time . ', End time: ' . $driverStatus->end_time . '. Contact admin to fix the error';
                                return Request::sendMessage($data);
                            }
                        }
                    }
                }

                $text = '';

                // no break
            case 2:
                if ($text === '') {
                    $notes['state'] = 2;
                    $this->conversation->update();
                    $data['text'] = BotHelper::t('A new load will be created');
                    // $load = Load::active()->where('pcs_number', $notes['pcs_number'])->orderBy('id', 'desc')->first();
                    // if ($load) {
                    //     $data['text'] = 'You are overwriting PCS number: ' . $load->pcs_number . ', Load ID: ' . $load->load_id . ' created by ' . $load->dsp_full_name . ' ' . Helper::formatDateTime($load->created_at);
                    // }
                    $puDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $notes['local_pickup_time']), $notes['pickup_timezone']);
                    $delDate = Helper::convertToBotTime(Carbon::createFromFormat('Y-m-d H:i', $notes['local_delivery_time']), $notes['delivery_timezone']);
                    $puDateForDriver = Carbon::createFromFormat('Y-m-d H:i', $notes['pickup_time_for_driver']);
                    $delDateForDriver = Carbon::createFromFormat('Y-m-d H:i', $notes['delivery_time_for_driver']);
                    $transitDays = $puDate->diffInDays($delDate);
                    if ($transitDays > Load::TRANSIT_MAX_DAYS) {
                        $data['text'] .= "\n" . 'Transit days too long - ' . $transitDays . ' days';
                    }
                    $transitDaysForDriver = $puDateForDriver->diffInDays($delDateForDriver);
                    if ($transitDaysForDriver > Load::TRANSIT_MAX_DAYS) {
                        $data['text'] .= "\n" . 'Transit days for driver too long - ' . $transitDaysForDriver . ' days';
                    }
                    $totalMile = floatval($notes['authorized_loaded_mile']) + floatval($notes['deadhead_mile']) + floatval($notes['authorized_mile']);
                    $rpm = $totalMile > 0 ? floatval($notes['rate']) / $totalMile : 0;
                    $data['text'] .= "\n" . 'RPM: ' . Helper::formatPrice($rpm);
                    $buttons = [
                        [BotHelper::t('Button Confirm'), BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }
                $text = '';

                // no break
            case 3:
                if ($message->getPhoto() == null && $message->getDocument() == null) {
                    $notes['state'] = 3;
                    $this->conversation->update();

                    $data['text'] = BotHelper::t('PDF file or photo');
                    $buttons = [
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);

                    $result = Request::sendMessage($data);
                    break;
                }

                if ($message->getDocument()) {
                    $notes['file_id'] = $message->getDocument()->getFileId();
                    $notes['file_type'] = Load::FILE_TYPE_DOCUMENT;
                } else {
                    $photos = $message->getPhoto();
                    $photo = array_pop($photos);
                    $notes['file_id'] = $photo->getFileId();
                    $notes['file_type'] = Load::FILE_TYPE_PHOTO;
                }
                $text = '';

                // no break
            case 4:
                // check load created before
                Load::where('pcs_number', $notes['pcs_number'])->update([
                    'dsp_bonus' => 0,
                    'active' => 0,
                    'status' => Load::STATUS_OVERWRITTEN,
                ]);

                // get driver
                $driver = null;
                if (!$notes['isThirdParty']) {
                    // $driver = Driver::firstOrCreate([
                    //     'name' => $notes['driver'],
                    // ], [
                    //     'dispatcher_id' => $user_id,
                    // ]);
                    $driver = Driver::active()->where('name', $notes['driver'])->first();

                    if (!$driver->dispatcher_id) {
                        $driver->update([
                            'dispatcher_id' => $user_id,
                        ]);
                    }
                }

                $load = Load::create([
                    'dsp_bot_user_id' => $user_id,
                    'assigned_dsp_bot_user_id' => $driver ? $driver->dispatcher_id : null,
                    'type' => $notes['type'],
                    'company_code' => $notes['company_code'],
                    'load_id' => $notes['load_id'],
                    'driver_id' => $driver ? $driver->id : null,
                    'team_id' => $driver ? $driver->team_id : null,
                    'third_company' => $notes['third_company'] ?? null,
                    'pcs_number' => $notes['pcs_number'],
                    'reference_no' => $notes['reference_no'],
                    'file_type' => $notes['file_type'],
                    'file_id' => $notes['file_id'],
                    'rate' => $notes['rate'],
                    'dsp_bonus' => 1,
                    'status' => Load::STATUS_CREATED,
                    'special_note' => $notes['special_note'],
                    'authorized_loaded_mile' => $notes['authorized_loaded_mile'],
                    'deadhead_mile' => $notes['deadhead_mile'],
                    'authorized_mile' => $notes['authorized_mile'],
                    'unauthorized_mile' => $notes['unauthorized_mile'],
                    'pickup_location' => $notes['pickup_location'],
                    'pickup_timezone' => $notes['pickup_timezone'],
                    'local_pickup_time' => $notes['local_pickup_time'],
                    'delivery_location' => $notes['delivery_location'],
                    'local_delivery_time' => $notes['local_delivery_time'],
                    'delivery_timezone' => $notes['delivery_timezone'],
                    'check_in_as' => $notes['check_in_as'],
                    'trailer' => $notes['trailer'],
                    'trailer_note' => $notes['trailer_note'],
                    'pickup_additional_stops' => $notes['pickup_additional_stops'],
                    'pickup_time_for_driver' => $notes['pickup_time_for_driver'],
                    'delivery_time_for_driver' => $notes['delivery_time_for_driver'],
                    'warnings' => $notes['warnings'],
                ]);

                // stop conversation
                $notes = [];
                $this->conversation->update();
                $this->conversation->stop();

                // send message
                $data['text'] = BotHelper::t('Load has been created');
                $data['reply_markup'] = StartCommand::getKeyboard($user_id);
                $result = Request::sendMessage($data);
                break;
        }

        return $result;
    }
}
