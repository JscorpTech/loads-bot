<?php

/**
 * This file is part of the PHP Telegram Bot example-bot package.
 * https://github.com/php-telegram-bot/example-bot/
 *
 * (c) PHP Telegram Bot Team
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Callback query command
 *
 * This command handles all callback queries sent via inline keyboard buttons.
 *
 * @see InlinekeyboardCommand.php
 */

namespace Longman\TelegramBot\Commands\SystemCommands;

use App\Helpers\BotHelper;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Request;

class CallbackqueryCommand extends SystemCommand
{
    /**
     * @var string
     */
    protected $name = 'callbackquery';

    /**
     * @var string
     */
    protected $description = 'Handle the callback query';

    /**
     * @var string
     */
    protected $version = '1.2.0';

    /**
     * Main command execution
     *
     * @return ServerResponse
     * @throws \Exception
     */
    public function execute(): ServerResponse
    {

        $update = $this->getUpdate()->getRawData();
        $callback_query = $this->getCallbackQuery();
        $callback_query_id = $callback_query->getId();
        $callback_data = $callback_query->getData();

        $message = $callback_query->getMessage();
        $message_id = $message->getMessageId();
        $chat_id = $message->getChat()->getId();

        $command = BotHelper::getCallbackCommand($callback_data);

        if ($command) {
            $update['message'] = $update['callback_query']['message'];
            $update['message']['from'] = $update['callback_query']['from'];
            $update['message']['text'] = '/' . $command['command'] . ' ' . $callback_data;
            $commandClass = $command['class'];
            return (new $commandClass($this->telegram, new Update($update)))->preExecute();
        }

        return Request::emptyResponse();

        // Callback query data can be fetched and handled accordingly.
        // $callback_query = $this->getCallbackQuery();
        // $callback_data  = $callback_query->getData();

        // return $callback_query->answer([
        //     'text'       => 'Content of the callback data: ' . $callback_data,
        //     'show_alert' => (bool) random_int(0, 1), // Randomly show (or not) as an alert.
        //     'cache_time' => 5,
        // ]);
    }
}
