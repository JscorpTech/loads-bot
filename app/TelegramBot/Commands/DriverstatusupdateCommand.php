<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\DriverStatus;
use App\Models\Load;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class DriverstatusupdateCommand extends UserCommand
{
    protected $name = 'driverstatusupdate';
    protected $description = 'Driver status update command';
    protected $usage = '/driverstatusupdate';
    protected $version = '1.0.0';

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = (string)$message->getMessageId();
        $chat_id = (string)$message->getChat()->getId();
        $from = $message->getFrom();
        $user_id = (string)$from->getId();
        $text = trim($message->getText(true));

        $driver = Driver::where('chat_id', $chat_id)->with('team.managers.botUser')->first();
        if (!$driver) {
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => 'Driver not found',
            ]);
        }

        $sendMessage = '';
        $lines = explode("\n", str_replace("\r", '', $text));
        $hashtag = trim($lines[0]);
        $notifyManagers = false;

        if ($hashtag == '#status') {
            // $date = Carbon::parse('2023-09-18 14:40');
            // $driverStatus = $driver->getCurrentDriverStatus($date);
            $driverStatus = $driver->getCurrentDriverStatus();
            $sendMessage = 'Status: ' . $driverStatus->status_title . "\n" . 'Start: ' . $driverStatus->start_time . "\n" . 'End: ' . $driverStatus->end_time . "\n" . 'Hashtag: ' . $driverStatus->hashtag . "\n" . 'Comment: ' . $driverStatus->comment;
        } elseif ($hashtag == '#trailerproblem') {
            $sendMessage = 'Incorrect format' . "\n" . Helper::getHashtagMessageExample($hashtag);
            if (!empty($lines[1]) && Str::startsWith($lines[1], 'Start:')) {
                $time = trim(str_replace('Start:', '', $lines[1]));
                try {
                    $date = Carbon::createFromFormat('Y-m-d H:i', $time);
                } catch (\Throwable $th) {
                    $date = null;
                    //throw $th;
                }
                if ($date) {
                    $driverStatus = $driver->getCurrentDriverStatus($date);
                    if ($driverStatus && $driverStatus->isShop() && $driverStatus->end_time == null) {
                        $sendMessage = 'Already shop - ' . $driverStatus->hashtag . ' - ' . $driverStatus->created_at->format('Y-m-d H:i');
                    } elseif ($driverStatus && $driverStatus->start_time > $date) {
                        $sendMessage = 'Incorrect time. Current status "' . $driverStatus->status_title . '" started at ' . $driverStatus->start_time->format('Y-m-d H:i');
                    } else {
                        // end previous status
                        $relatedLoad = null;
                        if ($driverStatus) {
                            if ($driverStatus->relatedload) {
                                $relatedLoad = $driverStatus->relatedload;
                            }
                            $driverStatus->end_time = (clone $date)->subSecond();
                            $driverStatus->save();
                        }
                        // start trailer problem shop status
                        $driverStatus = $driver->driverStatuses()->create([
                            'status' => DriverStatus::STATUS_SHOP,
                            'start_time' => $date,
                            'end_time' => null,
                            'hashtag' => $hashtag,
                            'comment' => ($relatedLoad && $relatedLoad->trailer) ? 'Trailer number: ' . $relatedLoad->trailer->trailer_number : '',
                        ]);
                        $driver->status = $driverStatus->status;
                        $driver->current_driver_status_id = $driverStatus->id;
                        $driver->saveQuietly();
                        $sendMessage = 'Shop started (' . $driverStatus->hashtag . '): ' . $date->format('Y-m-d H:i');
                        $notifyManagers = true;
                    }
                }
            }
        } elseif ($hashtag == '#trailerproblemsolved') {
            $sendMessage = 'Incorrect format' . "\n" . Helper::getHashtagMessageExample($hashtag);
            if (!empty($lines[1]) && Str::startsWith($lines[1], 'End:') && !empty($lines[2]) && Str::startsWith($lines[2], 'Shop address:')) {
                $time = trim(str_replace('End:', '', $lines[1]));
                try {
                    $date = Carbon::createFromFormat('Y-m-d H:i', $time);
                } catch (\Throwable $th) {
                    $date = null;
                    //throw $th;
                }
                $address = trim(str_replace('Shop address:', '', $lines[2]));
                $addressValid = false;
                if (preg_match(Helper::addressRegex(), $address)) {
                    $addressValid = true;
                }
                if ($date && $addressValid) {
                    $driverStatus = $driver->getCurrentDriverStatus($date);
                    if ($driverStatus && !$driverStatus->isShop()) {
                        $sendMessage = 'Not shop';
                    } elseif ($driverStatus && $driverStatus->isShop() && $driverStatus->hashtag != '#trailerproblem') {
                        $sendMessage = 'Not trailer problem';
                    } elseif ($driverStatus && $driverStatus->isShop() && $driverStatus->start_time > $date) {
                        $sendMessage = 'Incorrect time. Shop started at ' . $driverStatus->start_time->format('Y-m-d H:i');
                    } elseif ($driverStatus && $driverStatus->isShop() && $driverStatus->end_time == null) {
                        // end shop status
                        $driverStatus->end_time = (clone $date)->subSecond();
                        $driverStatus->shop_address = $address;
                        $driverStatus->save();
                        $oldDriverStatus = $driverStatus;
                        // start ready status
                        $driverStatus = $driver->driverStatuses()->create([
                            'status' => DriverStatus::STATUS_READY_1,
                            'start_time' => $date,
                            'end_time' => (clone $date)->addHours(3),
                            'shop_address' => $address,
                        ]);
                        $driver->status = $driverStatus->status;
                        $driver->current_driver_status_id = $driverStatus->id;
                        $driver->saveQuietly();
                        $sendMessage = 'Shop ended (' . $oldDriverStatus->hashtag . '): ' . $date->format('Y-m-d H:i');
                        $notifyManagers = true;
                    } else {
                        $sendMessage = 'Error';
                    }
                }
            }
        } elseif ($hashtag == '#truckproblem') {
            $sendMessage = 'Incorrect format' . "\n" . Helper::getHashtagMessageExample($hashtag);
            if (!empty($lines[1]) && Str::startsWith($lines[1], 'Start:')) {
                $time = trim(str_replace('Start:', '', $lines[1]));
                try {
                    $date = Carbon::createFromFormat('Y-m-d H:i', $time);
                } catch (\Throwable $th) {
                    $date = null;
                    //throw $th;
                }
                if ($date) {
                    $driverStatus = $driver->getCurrentDriverStatus($date);
                    if ($driverStatus && $driverStatus->isShop() && $driverStatus->end_time == null) {
                        $sendMessage = 'Already shop - ' . $driverStatus->hashtag . ' - ' . $driverStatus->start_time;
                    } elseif ($driverStatus && $driverStatus->start_time > $date) {
                        $sendMessage = 'Incorrect time. Current status "' . $driverStatus->status_title . '" started at ' . $driverStatus->start_time->format('Y-m-d H:i');
                    } else {
                        // end previous status
                        if ($driverStatus) {
                            $driverStatus->end_time = (clone $date)->subSecond();
                            $driverStatus->save();
                        }
                        // start truck problem shop status
                        $driverStatus = $driver->driverStatuses()->create([
                            'status' => DriverStatus::STATUS_SHOP,
                            'start_time' => $date,
                            'end_time' => null,
                            'hashtag' => $hashtag,
                            'comment' => 'Truck number: ' . $driver->truck_number,
                        ]);
                        $driver->status = $driverStatus->status;
                        $driver->current_driver_status_id = $driverStatus->id;
                        $driver->saveQuietly();
                        $sendMessage = 'Shop started (' . $driverStatus->hashtag . '): ' . $date->format('Y-m-d H:i');
                        $notifyManagers = true;
                    }
                }
            }
        } elseif ($hashtag == '#truckproblemsolved') {
            $sendMessage = 'Incorrect format' . "\n" . Helper::getHashtagMessageExample($hashtag);
            if (!empty($lines[1]) && Str::startsWith($lines[1], 'End:') && !empty($lines[2]) && Str::startsWith($lines[2], 'Shop address:')) {
                $time = trim(str_replace('End:', '', $lines[1]));
                try {
                    $date = Carbon::createFromFormat('Y-m-d H:i', $time);
                } catch (\Throwable $th) {
                    $date = null;
                    //throw $th;
                }
                $address = trim(str_replace('Shop address:', '', $lines[2]));
                $addressValid = false;
                if (preg_match(Helper::addressRegex(), $address)) {
                    $addressValid = true;
                }
                if ($date && $addressValid) {
                    $driverStatus = $driver->getCurrentDriverStatus($date);
                    if ($driverStatus && !$driverStatus->isShop()) {
                        $sendMessage = 'Not shop';
                    } elseif ($driverStatus && $driverStatus->isShop() && $driverStatus->hashtag != '#truckproblem') {
                        $sendMessage = 'Not truck problem';
                    } elseif ($driverStatus && $driverStatus->isShop() && $driverStatus->start_time > $date) {
                        $sendMessage = 'Incorrect time. Shop started at ' . $driverStatus->start_time->format('Y-m-d H:i');
                    } elseif ($driverStatus && $driverStatus->isShop() && $driverStatus->end_time == null) {
                        // end shop status
                        $driverStatus->end_time = (clone $date)->subSecond();
                        $driverStatus->shop_address = $address;
                        $driverStatus->save();
                        $oldDriverStatus = $driverStatus;
                        // start ready status
                        $driverStatus = $driver->driverStatuses()->create([
                            'status' => DriverStatus::STATUS_READY_1,
                            'start_time' => $date,
                            'end_time' => (clone $date)->addHours(3),
                            'shop_address' => $address,
                        ]);
                        $driver->status = $driverStatus->status;
                        $driver->current_driver_status_id = $driverStatus->id;
                        $driver->saveQuietly();
                        $sendMessage = 'Shop ended (' . $oldDriverStatus->hashtag . '): ' . $date->format('Y-m-d H:i');
                        $notifyManagers = true;
                    } else {
                        $sendMessage = 'Error';
                    }
                }
            }
        } elseif (in_array($hashtag, ['#resttime', '#hometime', '#vacation'])) {
            $sendMessage = 'Incorrect format' . "\n" . Helper::getHashtagMessageExample($hashtag);
            $newStatus = DriverStatus::STATUS_REST;
            if ($hashtag == '#hometime') {
                $newStatus = DriverStatus::STATUS_HOME;
            } elseif ($hashtag == '#vacation') {
                $newStatus = DriverStatus::STATUS_VACATION;
            }
            $newStatusTitle = DriverStatus::statuses()[$newStatus];

            if (!empty($lines[1]) && Str::startsWith($lines[1], 'Start:') && !empty($lines[2]) && Str::startsWith($lines[2], 'End:')) {
                $start = trim(str_replace('Start:', '', $lines[1]));
                $end = trim(str_replace('End:', '', $lines[2]));
                try {
                    $startDate = Carbon::createFromFormat('Y-m-d H:i', $start);
                    $endDate = Carbon::createFromFormat('Y-m-d H:i', $end);
                } catch (\Throwable $th) {
                    $startDate = null;
                    $endDate = null;
                    //throw $th;
                }
                if ($startDate && $endDate && $startDate < $endDate) {
                    $driverStatus = $driver->getCurrentDriverStatus($startDate);
                    if ($driverStatus && $driverStatus->isShop()) {
                        $sendMessage = 'Status is ' . $driverStatus->status_title . '. Can not change to ' . $newStatusTitle;
                    } else {
                        $loads = $driver
                            ->loads()
                            ->active()
                            ->where(function ($q1) use ($startDate, $endDate) {
                                $q1
                                ->where(function ($q2) use ($startDate) {
                                    $q2
                                        ->where('pickup_time', '<=', $startDate)
                                        ->where('delivery_time', '>=', $startDate);
                                })
                                ->orWhere(function ($q2) use ($startDate, $endDate) {
                                    $q2
                                        ->where('pickup_time', '>=', $startDate)
                                        ->where('pickup_time', '<=', $endDate);
                                });
                            })
                            ->count();
                        if ($loads > 0) {
                            $sendMessage = 'There are active loads between ' . $startDate->format('Y-m-d H:i') . ' and ' . $endDate->format('Y-m-d H:i') . '. Can not start ' . $newStatusTitle;
                        } else {
                            $driverStatus = $driver->driverStatuses()->create([
                                'status' => $newStatus,
                                'start_time' => $startDate,
                                'end_time' => $endDate,
                                'hashtag' => $hashtag,
                            ]);
                            $driver->status = $driverStatus->status;
                            $driver->current_driver_status_id = $driverStatus->id;
                            $driver->saveQuietly();
                            $sendMessage = $newStatusTitle . ': ' . $startDate->format('Y-m-d H:i') . ' - ' . $endDate->format('Y-m-d H:i');
                            $notifyManagers = true;
                        }
                    }
                }
            }
        }

        if ($sendMessage) {
            // notify group
            $res = Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => $sendMessage,
            ]);

            // notify managers chat
            if ($notifyManagers) {
                $managersMessage = $sendMessage;
                // notify managers chat
                $managersGroupChatId = Helper::setting('managers_group');
                $managers = null;
                if (in_array($hashtag, ['#trailerproblem', '#trailerproblemsolved'])) {
                    $managers = BotUserInfo::where('trailers_manager', 1)->with('botUser')->get();
                } elseif (in_array($hashtag, ['#truckproblem', '#truckproblemsolved'])) {
                    $managers = BotUserInfo::where('trucks_manager', 1)->with('botUser')->get();
                } elseif (in_array($hashtag, ['#resttime', '#hometime', '#vacation']) && !empty($driver->team->managers)) {
                    $managers = $driver->team->managers->where('dsp_manager', 1);
                }

                if ($managers && $managersGroupChatId) {
                    $managersMessage .= "\n";
                    $managersMessage .= 'Driver - ' . $driver->name . "\n";
                    foreach ($managers as $manager) {
                        if (!empty($manager->botUser->username)) {
                            $managersMessage .= ' @' . $manager->botUser->username;
                        }
                    }
                    Helper::toTelegram($managersMessage, $managersGroupChatId);
                }
            }
            return $res;
        }

        return Request::emptyResponse();
    }
}
