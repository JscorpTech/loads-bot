<?php

namespace App\TelegramBot\Commands;

use App\Models\Text;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class ContactsCommand extends UserCommand
{
    protected $name = 'contacts';
    protected $description = 'Contacts command';
    protected $usage = '/contacts';
    protected $version = '1.0.0';
    protected $need_mysql = true;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat = $message->getChat();
        $chat_id = $chat->getId();

        $sendText = Text::where('key', 'contacts')->first();

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => $sendText->description_for_telegram ?? __('Contacts'),
            'parse_mode' => 'HTML',
        ]);
    }
}
