<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Models\BotUserInfo;
use App\Models\Load;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\Update;
use Throwable;

class LoadsearchCommand extends UserCommand
{
    protected $name = 'loadsearch';
    protected $description = 'Load search command';
    protected $usage = '/loadsearch';
    protected $version = '1.0.0';
    protected $private_only = true;
    protected $conversation;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = (string)$message->getMessageId();
        $user = $message->getFrom();
        $user_id = (string)$user->getId();
        $chat = $message->getChat();
        $chat_id = (string)$chat->getId();
        $text = trim($message->getText(true));

        // Preparing Response
        $data = [
            'chat_id' => $chat_id,
        ];
        $result = Request::emptyResponse();

        // check other conversations
        $conversation = new Conversation($user_id, $chat_id);
        if ($conversation->exists() && $conversation->getCommand() != $this->getName()) {
            $conversation->notes = [];
            $conversation->update();
            $conversation->stop();
        }

        //Conversation start
        $this->conversation = new Conversation($user_id, $chat_id, $this->getName());

        $notes = &$this->conversation->notes;
        !is_array($notes) && $notes = [];

        //cache data from the tracking session if any
        $state = 0;
        if (isset($notes['state'])) {
            $state = $notes['state'];
        }

        // back
        if ($text == BotHelper::t('Button Back')) {
            $state--;
            $text = '';
        } elseif ($text == BotHelper::t('Button Cancel')) {
            $state = -1;
        }

        // cancel request
        if ($state == -1) {
            $notes = [];
            $this->conversation->update();
            $this->conversation->stop();
            return Request::sendMessage([
                'chat_id' => $chat_id,
                'text' => BotHelper::t('Welcome'),
                'reply_markup' => StartCommand::getKeyboard($user_id),
            ]);
        }

        switch ($state) {

            case 0:
                if ($text === '') {
                    $notes['state'] = 0;
                    $this->conversation->update();
                    $data['text'] = BotHelper::t('Enter Load ID or PCS number');
                    $buttons = [
                        [BotHelper::t('Button Back')],
                    ];
                    $data['reply_markup'] = (new Keyboard(...$buttons))
                        ->setResizeKeyboard(true)
                        ->setOneTimeKeyboard(true)
                        ->setSelective(true);
                    $result = Request::sendMessage($data);
                    break;
                }
            // no break
            case 1:
                // search load
                $loads = Load::where('load_id', $text)->orWhere('pcs_number', $text)->active()->orderBy('id', 'desc')->get();
                if ($loads->isEmpty()) {
                    // no loads found, continue search
                    $data['text'] = BotHelper::t('Load not found');
                } else {
                    // load found, stop search
                    $data['text'] = BotHelper::t('Loads found: ');
                    $buttons = [];
                    foreach ($loads as $load) {
                        $buttons[] = [
                            [
                                'text' => 'Load ID: ' . $load->load_id . ' (' . $load->created_at->format('Y-m-d H:i') . ')', 'callback_data' => 'loadshow:' . $load->id,
                            ]
                        ];
                    }
                    $data['reply_markup'] = new InlineKeyboard(...$buttons);
                }
                $result = Request::sendMessage($data);
        }

        return $result;
    }
}
