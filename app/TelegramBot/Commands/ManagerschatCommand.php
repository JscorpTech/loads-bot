<?php

namespace App\TelegramBot\Commands;

use App\Helpers\BotHelper;
use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\Load;
use App\Models\Setting;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Conversation;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\Update;
use Throwable;

class ManagerschatCommand extends UserCommand
{
    protected $name = 'managerschat';
    protected $description = 'Save managerschat chat';
    protected $usage = '/managerschat';
    protected $version = '1.0.0';

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $message_id = $message->getMessageId();
        $user = $message->getFrom();
        $user_id = (string)$user->getId();
        $chat = $message->getChat();
        $chat_id = (string)$chat->getId();
        $text = trim($message->getText(true));

        $userInfo = BotHelper::getUserInfo($user_id);

        $data = [
            'chat_id' => $chat_id,
        ];
        // check user active
        if (!$userInfo || !$userInfo->isActive()) {
            $data['text'] = BotHelper::t('Your account has not been activated yet');
            return Request::sendMessage($data);
        }

        // only Manager can save chat
        $canBeManager = false;
        if ($userInfo->isManager() || ($userInfo->user && $userInfo->user->isAdmin()) ) {
            $canBeManager = true;
        }
        if (!$canBeManager) {
            $data['text'] = 'You are not a manager';
            return Request::sendMessage($data);
        }

        Setting::where('key', 'managers_group')->update([
            'description' => $chat_id,
        ]);

        $data['text'] = 'Managers chat has been saved';
        return Request::sendMessage($data);
    }
}
