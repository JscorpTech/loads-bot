<?php

namespace App\TelegramBot\Commands;

use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\Text;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

class SosCommand extends UserCommand
{
    protected $name = 'sos';
    protected $description = 'Sos command';
    protected $usage = '/sos';
    protected $version = '1.0.0';
    protected $need_mysql = true;

    public function execute(): ServerResponse
    {
        $message = $this->getMessage();
        $chat = $message->getChat();
        $chat_id = $chat->getId();
        $chat_title = $chat->getTitle();

        $driver = Driver::where('chat_id', $chat_id)->with('team.managers.botUser')->orderBy('id', 'desc')->first();

        // notify managers chat
        $sendMessage = 'SOS!';
        if ($driver) {
            $sendMessage .= "\n" . 'Driver - ' . $driver->name;
        }
        $sendMessage .= "\n" . 'Chat - ' . $chat_title;
        $managersGroupChatId = Helper::setting('managers_group');
        $managers = BotUserInfo::where('relations_manager', 1)->with('botUser')->get();
        if ($managers->isNotEmpty()) {
            $sendMessage .= "\n";
            foreach ($managers as $manager) {
                if (!empty($manager->botUser) && !empty($manager->botUser->username)) {
                    $sendMessage .= ' @' . $manager->botUser->username;
                }
            }
        }
        Request::sendMessage([
            'chat_id' => $managersGroupChatId,
            'text' => $sendMessage,
        ]);

        return Request::sendMessage([
            'chat_id' => $chat_id,
            'text' => __('Managers have been notified'),
        ]);
    }
}
