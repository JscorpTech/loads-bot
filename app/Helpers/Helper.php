<?php

namespace App\Helpers;

use App\Models\Banner;
use App\Models\BotUserInfo;
use App\Models\Category;
use App\Models\Driver;
use App\Models\DriverStatus;
use App\Models\Load;
use App\Models\Page;
use App\Models\Setting;
use App\Models\StaticText;
use Carbon\CarbonInterface;
use Exception;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\DB;
use GuzzleHttp\Client;
use Longman\TelegramBot\Entities\File;

class Helper
{

    public static function formatPrice($number)
    {
        return '$' . static::formatNumber($number);
    }

    public static function formatNumber($number)
    {
        return number_format($number, 2, '.', ',');
    }

    public static function formatDate(Carbon $date, $year = false)
    {
        $yearFormat = ($year) ? ', Y' : '';
        return __($date->format('F')) . ' ' . $date->format('d' . $yearFormat);
    }

    public static function formatDateTime(Carbon $date)
    {
        return $date->format('Y-m-d H:i');
    }

    public static function formatDateSecond(Carbon $date)
    {
        return '<div>' . $date->format('d') . '</div><div>' . __($date->format('F')) . '</div>';
    }

    public static function setting($key)
    {
        $setting = Cache::remember('setting-' . $key, 300, function () use ($key) {
            return Setting::firstOrCreate(
                ['key' => $key],
                ['name' => $key, 'description' => '']
            );
        });
        return $setting->description;
    }

    public static function phone($phone)
    {
        return '+' . preg_replace('#[^\d]#', '', $phone);
    }

    public static function parsePhones($phones)
    {
        $parsed = [];
        $phones = str_replace([';'], ',', $phones);
        $phones = explode(',', $phones);
        foreach ($phones as $phone) {
            $parsed[] = [
                'original' => $phone,
                'clean' => self::phone($phone),
            ];
        }
        return $parsed;
    }

    public static function reformatText($text)
    {
        return preg_replace(['#\*(.*?)\*#', '#\#(.*?)\##', '#\|\|#'], ['<strong>$1</strong>', '<span class="text-primary">$1</span>', '<br>'], $text);
    }

    public static function formatWorkDays($days)
    {
        $days = explode(',', preg_replace('#[^0-9,]#', '', $days));
        $days = array_map('intval', $days);
        $daysStatus = [];
        for ($i = 1; $i <= 7; $i++) {
            $daysStatus[$i] = in_array($i, $days) ? true : false;
        }
        return $daysStatus;
    }

    /**
     * Return translated model.
     *
     * @param Illuminate\Database\Eloquent\Model $model
     *
     * @return Illuminate\Database\Eloquent\Model
     */
    public static function translation($model)
    {
        if (app()->getLocale() != config('voyager.multilingual.default')) {
            return $model->translate();
        }
        return $model;
    }

    private static function telegramClient()
    {
        $client = new Client([
            'base_uri' => 'https://api.telegram.org',
            'timeout'  => 2.0,
        ]);
        return $client;
    }

    /**
     * Send message via telegram bot to group
     */
    public static function toTelegram($text, $chat_id = '', $parse_mode = 'HTML', $replyMarkup = [])
    {
        $token = config('services.telegram.bot_token');

        if (!$chat_id) {
            $chat_id = config('services.telegram.chat_id');
        }

        $formData = [];
        $formData['chat_id'] = $chat_id;
        $formData['text'] = $text;
        if (in_array($parse_mode, ['HTML', 'Markdown'])) {
            $formData['parse_mode'] = $parse_mode;
        }
        if ($replyMarkup) {
            $formData['reply_markup'] = $replyMarkup;
        }

        try {
            $client = self::telegramClient();

            $client->post('/bot' . $token . '/sendMessage', [
                'form_params' => $formData,
            ]);
        } catch (Exception $e) {
        }
    }

    public static function downloadTelegramFile(File $file, $dir)
    {
        $tg_file_path = $file->getFilePath();
        $file_path    = $dir . '/' . $tg_file_path;

        $file_dir = dirname($file_path);
        //For safety reasons, first try to create the directory, then check that it exists.
        //This is in case some other process has created the folder in the meantime.
        if (!@mkdir($file_dir, 0755, true) && !is_dir($file_dir)) {
            throw new Exception('Directory ' . $file_dir . ' can\'t be created');
        }

        try {
            $client = self::telegramClient();
            $client->get(
                '/file/bot' . config('services.telegram.bot_token') . '/' . $tg_file_path,
                ['sink' => $file_path]
            );

            return filesize($file_path) > 0;
        } catch (Exception $e) {
            return false;
        }
    }

    public static function storeFile($model, $field, $dir, $isImage = false)
    {
        if (request()->has($field)) {
            $url = request()->$field->store($dir . '/' . date('FY'), 'public');
            if (!$isImage) {
                $url = json_encode([
                    [
                        'download_link' => $url,
                        'original_name' => request()->$field->getClientOriginalName(),
                    ]
                ]);
            }
            $model->update([
                $field => $url,
            ]);
        }
        return $model;
    }

    public static function storeImage($model, $field, $dir, $thumbs = [])
    {
        $model = self::storeFile($model, $field, $dir, true);
        if ($thumbs && $model->$field) {
            $image = Image::make(storage_path('app/public/' . $model->$field));
            if ($image) {
                $ext = mb_strrchr($model->$field, '.');
                $pos = mb_strrpos($model->$field, '.');
                $fileName = mb_substr($model->$field, 0, $pos);
                foreach ($thumbs as $key => $value) {
                    $image->fit($value[0], $value[1])->save(storage_path('app/public/' . $fileName . '-' . $key . $ext));
                }
            }
        }
        return $model;
    }

    public static function checkModelActive($model)
    {
        $className = get_class($model);
        if (!isset($model->status) || !defined("$className::STATUS_ACTIVE") || (int)$model->status !== $className::STATUS_ACTIVE) {
            abort(404);
        }
    }

    public static function replaceTemplates($text, $replacements = [])
    {
        if (!$replacements) {
            return $text;
        }
        return str_replace(
            array_map(
                function ($value) {
                    return '{' . $value . '}';
                },
                array_keys($replacements)
            ),
            array_values($replacements),
            $text
        );
    }

    public static function addInitialReview($model)
    {
        $data = [
            'name' => 'Админ',
            'body' => 'Отлично!',
            'rating' => 5,
            'status' => 1,
        ];
        $model->reviews()->create($data);
    }

    public static function sendSMS($messageId, $phoneNumber, $message)
    {
        Log::info($message);
        return true;
    }

    public static function messagePrefix()
    {
        $name = str_replace(' ', '', Str::lower(config('app.name')));
        $prefix = config('app.env') == 'production' ? $name : 'test' . $name;
        return $prefix;
    }

    public static function getTree($collection, $parent = null, $level = 1)
    {
        $filtered = $collection->filter(function ($value) use ($parent) {
            return $value['parent_id'] == $parent;
        });
        $filtered->map(function ($item) use ($collection, $level) {
            $item['children'] = self::getTree($collection, $item->id, $level + 1);
        });
        return $filtered;
    }

    public static function activeCategories($category, $ids = [])
    {
        $ids[] = $category->id;
        if ($category->parent) {
            $ids = self::activeCategories($category->parent, $ids);
        }
        return $ids;
    }

    public static function voyagerFileUrl($file)
    {
        $file = json_decode($file);
        if (!empty($file[0])) {
            return Storage::url($file[0]->download_link);
        }
        return false;
    }

    public static function validateDate($date, $separator = '.')
    {
        $pattern = '#^\s*(3[01]|[12][0-9]|0?[1-9])\\' . $separator . '(1[012]|0?[1-9])\\' . $separator . '((?:19|20)\d{2})\s*$#';
        return preg_match($pattern, $date);
    }

    public static function generateLoadsStats($loads)
    {
        $stats = [];
        $dsps = BotUserInfo::where('position', 'DSP')->orderBy('full_name')->get();
        foreach ($dsps as $dsp) {
            $dspLoads = $loads->where('dsp_bot_user_id', $dsp->user_id);
            $dspStats = [
                'dsp' => $dsp,
                'by_load_types' => [],
                'total_loads' => $dspLoads->count(),
                'total_loads_cancelled' => $dspLoads->where('status', Load::STATUS_CANCELLED)->count(),
            ];
            foreach (Load::types() as $key => $value) {
                $dspStats['by_load_types'][$key] = [
                    'loads' => $dspLoads->where('type', $key)->count(),
                    'loads_cancelled' => $dspLoads->where('type', $key)->where('status', Load::STATUS_CANCELLED)->count(),
                ];
            }
            $stats[] = $dspStats;
        }
        return $stats;
    }

    public static function loadStatsTelegramText($stats, $filter)
    {
        $text = __('DSP stats') . PHP_EOL;
        $text .= __('From') . ' ' . $filter['pickup_time_from']->format('Y-m-d H:i') . ' ' . __('To') . ' ' . $filter['pickup_time_to']->format('Y-m-d H:i') . PHP_EOL . PHP_EOL;
        foreach ($stats as $stat) {
            $text .= 'DSP ' . $stat['dsp']->full_name . PHP_EOL;
            $text .= 'Amazon Loads: ' . $stat['amazon_loads'] . ' Amazon Loads Cancelled: ' . $stat['amazon_loads_cancelled'] . PHP_EOL;
            $text .= 'Market Loads: ' . $stat['market_loads'] . ' Market Loads Cancelled: ' . $stat['market_loads_cancelled'] . PHP_EOL;
            $text .= 'Total Loads: ' . $stat['total_loads'] . ' Total Loads Cancelled: ' . $stat['total_loads_cancelled'] . PHP_EOL;
            $text .= PHP_EOL;
        }
        return $text;
    }

    public static function getDriverWeeklyStats(Driver $driver, CarbonInterface $weekStart, CarbonInterface $weekEnd)
    {
        $stats = [];
        $now = now();
        $driver->load(['loads' => function ($q) use ($weekStart, $weekEnd) {
            $q->active()->where('pickup_time', '>=', $weekStart)->where('pickup_time', '<=', $weekEnd);
        }]);
        $stats['loads'] = $driver->loads;

        $driverPlan = $driver->driverPlans()->where('start_time', $weekStart->format('Y-m-d H:i:s'))->orderBy('id', 'desc')->first();

        // set prices
        $stats['pay_per_mile'] = $driverPlan ? $driverPlan->pay_per_mile : $driver->pay_per_mile;
        $stats['penske_pay_per_mile'] = $driverPlan ? $driverPlan->penske_pay_per_mile : $driver->penske_pay_per_mile;
        $stats['fixed_cost'] = $driverPlan ? $driverPlan->fixed_cost : $driver->fixed_cost;
        $stats['fuel_price_per_mile'] = $driverPlan ? $driverPlan->fuel_price_per_mile : floatval(Helper::setting('fuel_price_per_mile'));
        $stats['toll_price_per_mile'] = $driverPlan ? $driverPlan->toll_price_per_mile : floatval(Helper::setting('toll_price_per_mile'));

        // set targets
        $stats['weeklyTarget'] = $driverPlan ? $driverPlan->target : $driver->weekly_target;
        $stats['weeklyTargetMile'] = $driverPlan ? $driverPlan->target_mile : $driver->weekly_target_mile;
        $stats['dayOfWeek'] = 0;
        $stats['weeklyTargetCurrentPercent'] = 100;
        if ($now > $weekStart && $now < $weekEnd) {
            $stats['dayOfWeek'] = $now->dayOfWeek;
            switch ($stats['dayOfWeek']) {
                case 6:
                    $stats['weeklyTargetCurrentPercent'] = 90;
                    break;

                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                    $stats['weeklyTargetCurrentPercent'] = $stats['dayOfWeek'] * 16;
                    break;
            }
        }

        // calculate stats
        $stats = static::calculateStats($stats);

        // calculate other statuses
        $stats['completedPercent'] = $stats['weeklyTarget'] > 0 ? round($stats['loadsTotal'] / $stats['weeklyTarget'] * 100) : 100;
        $stats['completionStatus'] = 'success';
        if (in_array($stats['dayOfWeek'], [1, 2, 3]) && $stats['completedPercent'] < $stats['weeklyTargetCurrentPercent']) {
            $stats['completionStatus'] = 'warning-dark';
        } elseif ($stats['completedPercent'] < $stats['weeklyTargetCurrentPercent']) {
            $stats['completionStatus'] = 'danger';
        }
        $stats['perMileStatus'] = 'success';
        if ($stats['perMile'] < 2) {
            $stats['perMileStatus'] = 'warning-dark';
        }
        return $stats;
    }

    public static function getDriverDailyStats(Driver $driver, CarbonInterface $day, $driverActiveLoads = null)
    {
        $stats = [];
        $dayStart = (clone $day)->startOfDay();
        $dayEnd = (clone $day)->endOfDay();
        if ($driverActiveLoads) {
            $loads = $driverActiveLoads->where('pickup_time', '>=', $dayStart)->where('pickup_time', '<=', $dayEnd);
        } else {
            $driver->load(['loads' => function ($q) use ($dayStart, $dayEnd) {
                $q->active()->where('pickup_time', '>=', $dayStart)->where('pickup_time', '<=', $dayEnd);
            }]);
            $loads = $driver->loads;
        }

        $stats['loads'] = $loads;

        // calculate stats
        $stats = static::calculateStats($stats);

        return $stats;
    }

    private static function calculateStats(array $stats)
    {
        $stats['loadsRate'] = $stats['loads']->whereIn('status', [Load::STATUS_CREATED, Load::STATUS_CONFIRMED, Load::STATUS_RECONFIRMED])->sum('rate');
        $stats['loadsTonu'] = $stats['loads']->whereIn('status', [Load::STATUS_CANCELLED])->sum('tonu');
        $stats['loadsTotal'] = $stats['loads']->sum('total');

        $stats['loadsAuthorizedLoadedMile'] = $stats['loads']->whereIn('status', [Load::STATUS_CREATED, Load::STATUS_CONFIRMED, Load::STATUS_RECONFIRMED])->sum('authorized_loaded_mile');
        $stats['loadsDeadheadMile'] = $stats['loads']->whereIn('status', [Load::STATUS_CREATED, Load::STATUS_CONFIRMED, Load::STATUS_RECONFIRMED])->sum('deadhead_mile');
        $stats['loadsAuthorizedMile'] = $stats['loads']->whereIn('status', [Load::STATUS_CREATED, Load::STATUS_CONFIRMED, Load::STATUS_RECONFIRMED])->sum('authorized_mile');
        $stats['loadsUnauthorizedMile'] = $stats['loads']->whereIn('status', [Load::STATUS_CREATED, Load::STATUS_CONFIRMED, Load::STATUS_RECONFIRMED])->sum('unauthorized_mile');
        $stats['loadsMile'] = $stats['loads']->sum('mile');

        $stats['perMile'] = $stats['loadsMile'] > 0 ? round($stats['loadsTotal'] / $stats['loadsMile'], 2) : 0;

        if (isset($stats['pay_per_mile']) && isset($stats['penske_pay_per_mile']) && isset($stats['fixed_cost']) && isset($stats['fuel_price_per_mile']) && isset($stats['toll_price_per_mile'])) {
            $stats['profit'] = $stats['loadsTotal'] - $stats['loadsMile'] * $stats['pay_per_mile'] - $stats['loadsMile'] * $stats['penske_pay_per_mile'] - $stats['fixed_cost'] - $stats['loadsMile'] * $stats['fuel_price_per_mile'] - $stats['loadsMile'] * $stats['toll_price_per_mile'];
        }

        return $stats;
    }

    public static function exampleAddress()
    {
        return '4133 Veterans Memorial Drive, Batavia, NY 14020';
    }

    public static function addressRegex()
    {
        // return '/.*?, .*?,\s\w{2}\s\d{5}(-\d{4})?$/'; // zip code required
        return '/.*?, .*?,\s\w{2}(?:\s\d{4,5})?$/'; // zip code not required
    }

    public static function parseAddress($address)
    {
        $address = (string)$address;
        $data = [
            'address' => '',
            'city' => '',
            'state' => '',
            'zip' => '',
        ];
        $raw = explode(',', $address);

        // check old or new format
        $oldFormat = false;
        $first = trim($raw[0]);
        if (preg_match('/^[A-Z]{2}$/', $first)) {
            $oldFormat = true;
        }

        if ($oldFormat) {
            // old format
            $data['state'] = trim($raw[0]);
            if (!empty($raw[1])) {
                $data['city'] = trim($raw[1]);
            }
            if (!empty($raw[2])) {
                $data['address'] = trim($raw[2]);
            }
        } else {
            // new format
            $data['address'] = trim($raw[0]);
            if (!empty($raw[1])) {
                $data['city'] = trim($raw[1]);
            }
            if (!empty($raw[2])) {
                $raw2raw = explode(' ', trim($raw[2]));
                $data['state'] = trim($raw2raw[0]);
                if (!empty($raw2raw[1])) {
                    $data['zip'] = trim($raw2raw[1]);
                }
            }
        }
        return $data;
    }

    public static function groupHashtagMessages()
    {
        return [
            '#sos' => '',
            '#status' => '',
            '#trailerproblem' => 'Start: 2023-09-12 21:50',
            '#trailerproblemsolved' => 'End: 2023-09-12 21:50' . "\n" . 'Shop address: ' . Helper::exampleAddress(),
            '#truckproblem' => 'Start: 2023-09-12 21:50',
            '#truckproblemsolved' => 'End: 2023-09-12 21:50' . "\n" . 'Shop address: ' . Helper::exampleAddress(),
            '#resttime' => 'Start: 2023-09-12 22:30' . "\n" . 'End: 2023-09-13 06:30',
            '#hometime' => 'Start: 2023-09-16 08:30' . "\n" . 'End: 2023-09-19 08:30',
            '#vacation' => 'Start: 2023-09-19 08:30' . "\n" . 'End: 2023-10-15 08:30',
        ];
    }

    public static function getHashtagMessageExample($hashtag)
    {
        $message = 'Hashtag is not supported';
        if (isset(static::groupHashtagMessages()[$hashtag])) {
            $message = 'Correct message format:' . "\n\n";
            $message .= $hashtag . "\n";
            $message .= static::groupHashtagMessages()[$hashtag];
        }
        return $message;
    }

    public static function getDelState(Driver $driver)
    {
        $address = null;
        $load = $driver->relatedload;
        if ($load) {
            $address = $load->delivery_location;
        }
        $driverStatus = $driver->currentDriverStatus;
        if ($driverStatus && $driverStatus->isHome()) {
            $address = $driver->home_address;
        }
        if ($driverStatus && $driverStatus->shop_address) {
            $address = $driverStatus->shop_address;
        }
        $state = '-';
        if ($address) {
            $address = static::parseAddress($address);
            $state = trim($address['state']);
        }
        return $state;
    }

    public static function getPuState(Driver $driver)
    {
        $address = null;
        $load = $driver->relatedload;
        if ($load) {
            $address = $load->pickup_location;
        }
        $state = '-';
        if ($address) {
            $address = static::parseAddress($address);
            $state = trim($address['state']);
        }
        return $state;
    }

    public static function statesList()
    {
        return [
            'AL', 'AK', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'FL', 'GA', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'OH', 'OK', 'OR', 'PA', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VA', 'WA', 'WV', 'WI', 'WY'
        ];
    }

    public static function driverStatuses()
    {
        return DriverStatus::statuses();
    }

    public static function convertToBotTime(CarbonInterface $date, $fromTimezone)
    {
        // convert to bot time - eastern time
        switch ($fromTimezone) {
            case 'CST':
            case 'CDT':
                $date->addHours(1);
                break;
            case 'MST':
            case 'MDT':
                $date->addHours(2);
                break;
            case 'PST':
            case 'PDT':
                $date->addHours(3);
                break;
        }
        return $date;
    }

    public static function getLoadBatchNumber(Load $load)
    {
        $start = Carbon::parse('2017-10-16');
        return $start->diffInWeeks($load->pickup_time);
    }
}
