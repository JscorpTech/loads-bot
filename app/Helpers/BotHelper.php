<?php


namespace App\Helpers;

use App\Models\BotUserInfo;
use App\Models\Load;
use App\TelegramBot\Commands\LoadeditCommand;
use App\TelegramBot\Commands\LoadshowCommand;
use App\TelegramBot\Commands\LoadupdateCommand;
use Illuminate\Support\Facades\Log;
use Longman\TelegramBot\Commands\UserCommands\ApartmentCommand;
use Longman\TelegramBot\Commands\UserCommands\CartCommand;
use Longman\TelegramBot\Commands\UserCommands\CategoryCommand;
use Longman\TelegramBot\Commands\UserCommands\EditmessagereplymarkupCommand;
use Longman\TelegramBot\Commands\UserCommands\EnterserialnumberCommand;
use Longman\TelegramBot\Commands\UserCommands\FloorCommand;
use Longman\TelegramBot\Commands\UserCommands\MyordersCommand;
use Longman\TelegramBot\Commands\UserCommands\OrderCommand;
use Longman\TelegramBot\Commands\UserCommands\OrdershowCommand;
use Longman\TelegramBot\Commands\UserCommands\PayonlineCommand;
use Longman\TelegramBot\Commands\UserCommands\ProductCommand;
use Longman\TelegramBot\Commands\UserCommands\ReviewCommand;
use Longman\TelegramBot\Commands\UserCommands\SendrequestCommand;

class BotHelper
{
    public static function t($key, $lang = 'en')
    {
        return self::translations()[$lang][$key] ?? $key;
    }

    public static function parseCallbackData($callback_data)
    {
        $data = [];
        $callback_data = explode('|', $callback_data);
        foreach ($callback_data as $key => $value) {
            $row = explode(':', $value);
            $data[$row[0]] = isset($row[1]) ? $row[1] : '';
        }
        return $data;
    }

    public static function getCallbackCommand($data)
    {
        $command = [];
        $data = self::parseCallbackData($data);
        foreach (self::keyCommands() as $key => $value) {
            if (isset($data[$key])) {
                $command = $value;
                break;
            }
        }
        return $command;
    }

    public static function getUserInfo($user_id)
    {
        return BotUserInfo::where('user_id', $user_id)->with('user')->first();
    }

    public static function getUserLanguage($pdo, $user_id)
    {
        $userInfo = self::getUserInfo($pdo, $user_id);
        return $userInfo['language'] ?? app()->getLocale();
    }

    public static function reformatPhoneNumber($phoneNumber)
    {
        $phoneNumber = preg_replace('#[^\d]#', '', $phoneNumber);
        if (mb_strlen($phoneNumber) == 9) {
            $phoneNumber = '+998' . $phoneNumber;
        } elseif (mb_strlen($phoneNumber) == 12) {
            $phoneNumber = '+' . $phoneNumber;
        } else {
            //return '';
        }
        return $phoneNumber;
    }

    public static function formatPrice($price, $lang = 'ru')
    {
        return number_format($price, 0, '.', ' ') . ' ' . BotHelper::t('Currency sum', $lang);
    }

    public static function ppp($text)
    {
        file_put_contents('ppp.txt', print_r($text, true));
    }

    public static function ppp2($text)
    {
        file_put_contents('ppp2.txt', print_r($text, true));
    }

    public static function keyCommands()
    {
        return [
            'loadshow' => [
                'command' => 'loadshow',
                'class' => LoadshowCommand::class
            ],
            'loadedit' => [
                'command' => 'loadedit',
                'class' => LoadeditCommand::class
            ],
            'loadupdate' => [
                'command' => 'loadupdate',
                'class' => LoadupdateCommand::class
            ],
        ];
    }

    public static function translations()
    {
        return [
            'common' => [
                'Button Russian' => '🇷🇺 Русский',
                'Button Uzbek' => '🇺🇿 Oʻzbekcha',
                'Choose language' => 'Выберите язык / Tilni tanlang',
                '' => '',
            ],
            'en' => [
                'Button Amazon Loads' => '🚚 Amazon Loads',
                'Button Back' => '⬅️ Back',
                'Button Cancel' => '✖️ Cancel',
                'Button Confirm' => '✔️ Confirm',
                'Button Loads' => '🚚 Loads',
                'Button Market Loads' => '🚚 Market Loads',
                'Button New Load' => '🆕 New Load',
            ],
            'ru' => [
                'Access denied' => 'Доступ запрещен',
                'Add to cart' => 'Добавить в корзину',
                'Added to cart' => 'Добавлен в корзину',
                'Address' => 'Адрес',
                'Address saved' => 'Адрес сохранен',
                'Apartment' => 'Квартира',
                'Attached' => 'Прикреплено',
                'Back' => 'Назад',
                'Button Accept' => '✔️ Принимаю',
                'Button Back' => '⬅️ Назад',
                'Button Cancel' => '✖️ Отменить',
                'Button Cart' => '🛒 Корзина',
                'Button Cart Clear' => '❌ Очистить корзину',
                'Button Catalog' => '📂 Каталог',
                'Button Change address' => '🏡 Изменить адрес',
                'Button Change language' => '🏳️ Изменить язык',
                'Button Change phone number' => '📞 Изменить номер телефона',
                'Button Commercial real estate' => '🏬 Коммерческая недвижимость',
                'Button Computers' => '🖥 Компьютеры',
                'Button Confirm' => '✔️ Подтвердить',
                'Button Delivery' => '🚙 Доставка',
                'Button Feedback' => '☎️ Обратная связь',
                'Button Free delivery' => '🚗 Бесплатная доставка',
                'Button Help' => '❓ Помощь',
                'Button Home' => '🏠 Главная старница',
                'Button Info Aparto' => '📋 Info Aparto',
                'Button Laptops' => '💻 Ноутбуки',
                'Button Leave a review' => '✍️ Оставить отзыв',
                'Button Monoblocks' => '🖥 Моноблоки',
                'Button My orders' => '🥡 Мои заказы',
                'Button Next' => 'Дальше ➡️',
                'Button No' => '✖️ Нет',
                'Button Order' => '🛍 Заказать',
                'Button Orders' => '🥡 Заказы',
                'Button Our products' => '🏅 Наша продукция',
                'Button Pickup' => '🏃 Самовывоз',
                'Button Reject' => '✖️ Не принимаю',
                'Button Residential real estate' => '🏢 Жилая недвижимость',
                'Button Reviews' => '💬 Отзывы',
                'Button Send message' => '✉️Отправить сообщение',
                'Button Send my number' => 'Отправить мой номер',
                'Button Send request' => '✏️ Оставить заявку',
                'Button Service price' => '💵 Стоимость услуг',
                'Button Settings' => '⚙️ Настройки',
                'Button Yes' => '✔️ Да',
                'Cancelled' => 'Отменен',
                'Cart' => 'Корзина',
                'Cart is empty' => 'Корзина пуста',
                'Catalog' => 'Каталог',
                'Check and cofirm your order' => 'Проверьте и подтвердите заказ',
                'Checkout' => 'Оформить заказ',
                'Choose apartment' => 'Выберите квартиру',
                'Choose category' => 'Выберите категорию',
                'Choose delivery type 2' => 'Заберите свой заказ самостоятельно 🙋‍♂️ или выберите доставку 🚙',
                'Choose floor' => 'Выберите этаж',
                'Choose or write delivery method' => 'Выберите или напишите способ доставки',
                'Choose order' => 'Выберите заказ',
                'Choose payment method' => 'Выберите способ оплаты',
                'Choose premises' => 'Выберите помещение',
                'Choose product' => 'Выберите продукт',
                'Choose quantity' => 'Выберите количество',
                'Choose setting' => 'Выберите настройку',
                'Code is incorrect' => 'Неправильный код',
                'Commercial real estate' => 'Коммерческая недвижимость',
                'Contact phone number' => 'Номер телефона для контакта',
                'Cost' => 'Стоимость',
                'Currency sum' => 'сум',
                'Delivery' => 'Доставка',
                'Delivery self pickup' => 'Самовывоз',
                'Enter delivery address' => 'Напишите адрес доставки',
                'Enter delivery address or send location' => 'Напишите адрес доставки или отправьте локацию',
                'Enter new address' => 'Введите новый адрес',
                'Enter new phone number' => 'Введите новый номер телефона',
                'Get price' => 'Узнать цену',
                'Go to cart' => 'Перейти в корзину',
                'Go to orders' => 'К заказам',
                'in format' => 'в формате %s',
                'is dayoff' => 'нерабочий день',
                'Last orders list' => 'Список последних заказов',
                'Location' => 'Локация',
                'Menu' => 'Меню',
                'Name' => 'Имя',
                'Name of badges' => 'Наименование нагрудных знаков',
                'No information' => 'Нет информации',
                'No location' => 'Нет локации',
                'No more products in the category. Return' => 'В этой категории товаров нет. Вернуться назад',
                'No more products. Return last page' => 'Больше товаров нет. Вернуться на пред. стр.',
                'No orders' => 'Заказов нет',
                'No products in cart' => 'Корзина пуста',
                'New order' => 'Новый заказ',
                'New request' => 'Новый запрос',
                'Order accepted' => 'Заказ принят',
                'Order cancelled' => 'Заказ отменен',
                'Order details' => 'Детали заказа',
                'Order ID' => 'Номер заказа',
                'Order not found' => 'Заказ не найден',
                'Order sum incorrect' => 'Сумма заказа неправильная',
                'Order are accepted from to' => 'Заказы принимаются от %s до %s',
                'Our products' => 'Наша продукция',
                'Pay online' => 'Оплата онлайн',
                'Pay with Click' => 'Оплатить с Click',
                'Pay with Payme' => 'Оплатить с Payme',
                'Payment method' => 'Способ оплаты',
                'Phone number' => 'Номер телефона',
                'Phone number saved' => 'Номер телефона сохранен',
                'Photos attached' => 'Фото прикреплено',
                'Premises' => 'Помещение',
                'Price' => 'Цена',
                'Product' => 'Продукт',
                'Product added to cart' => 'Продукт добавлен в корзину',
                'Products' => 'Продукты',
                'Quantity' => 'Количество',
                'Rate our services' => 'Оцените наши услуги',
                'Rating 1' => '😤Хочу пожаловаться 👎🏻',
                'Rating 2' => '☹️Не понравилось, на 2 ⭐️⭐️',
                'Rating 3' => '😐Удовлетворительно, на 3 ⭐️⭐️⭐️',
                'Rating 4' => '☺️Нормально, на 4 ⭐️⭐️⭐️⭐️',
                'Rating 5' => '😊Все понравилось, на 5 ❤️',
                'Request accepted' => 'Запрос принят',
                'Request details' => 'Детали запроса',
                'Residential real estate' => 'Жилая недвижимость',
                'Review accepted' => 'Спасибо! Ваш отзыв принят',
                'Reviews' => 'Отзывы',
                'Security code' => 'Код проверки',
                'Send location' => 'Отправьте локацию',
                'Services cost' => 'Стоимость услуг',
                'Shipping price' => 'Стоимость доставки',
                'Shipping price depends on location text' => 'Стоимость доставки зависит от вашего местоположения и будет сообщена оператором после подтверждения заказа',
                'Show products' => 'Показать продукцию',
                'Sorry' => 'Извините',
                'Status' => 'Статус',
                'To Order' => 'Заказать',
                'To Send location' => 'Отправить локацию',
                'Total' => 'Итого',
                'Welcome' => 'Добро пожаловать!',
                'Write a review' => 'Напишите отзыв',
                'Write name of badges or send photo' => 'Если у вас есть фото прикрепите, затем напишите наименование нагрудных знаков. Пример: 25 лет ВС РУз, орден "Дружбы народов" , медаль " За отвагу".',
                'Your address' => 'Ваш адрес',
                'Your name' => 'Ваше имя',
                'Your firstname' => 'Ваше имя',
                'Your lastname' => 'Ваша фамилия',
                'Your phone number' => 'Ваш номер телефона',
                '' => '',
            ],
            'uz' => [
                'Add to cart' => 'Savatga qoʻshish',
                'Added to cart' => 'Savatga qoʻshildi',
                'Address' => 'Manzil',
                'Address saved' => 'Manzil saqlandi',
                'Apartment' => 'Kvartira',
                'Attached' => 'Прикреплено',
                'Back' => 'Orqaga',
                'Button Accept' => '✔️ Qabul qilaman',
                'Button Back' => '⬅️ Orqaga',
                'Button Cancel' => '✖️ Bekor qilish',
                'Button Cart' => '🛒 Savat',
                'Button Cart Clear' => '❌ Savatni tozalash',
                'Button Catalog' => '📂 Katalog',
                'Button Change address' => '🏡 Manzil',
                'Button Change language' => '🏳️ Til',
                'Button Change phone number' => '📞 Telefon raqami',
                'Button Commercial real estate' => '🏬 Tijorat koʻchmas mulki',
                'Button Computers' => '🖥 Kompyuterlar',
                'Button Confirm' => '✔️ Tasdiqlash',
                'Button Delivery' => '🚙 Yetkazib berish',
                'Button Feedback' => '☎️ Biz bilan aloqa',
                'Button Free delivery' => '🚗 Yetkazib berish bepul',
                'Button Help' => '❓ Yordam',
                'Button Home' => '🏠 Bosh sahifa',
                'Button Info Aparto' => '📋 Info Aparto',
                'Button Laptops' => '💻 Noutbuklar',
                'Button Leave a review' => '✍️ Fikr bildirish',
                'Button Monoblocks' => '🖥 Monobloklar',
                'Button My orders' => '📖 Buyurtmalarim',
                'Button Next' => 'Keyingi ➡️',
                'Button No' => '✖️ Yoʻq',
                'Button Order' => '🛍 Buyurtma berish',
                'Button Orders' => '🥡 Buyurtmalar',
                'Button Our products' => '🏅 Bizning mahsulotlarimiz',
                'Button Pickup' => '🏃 Olib ketish',
                'Button Reject' => '✖️ Qabul qilmayman',
                'Button Residential real estate' => '🏢 Turar-joy majmuasi',
                'Button Reviews' => '💬 Tavsiyalar',
                'Button Send message' => '✉️Xabar yuborish',
                'Button Send my number' => 'Telefon raqamimni yuborish',
                'Button Send request' => '✏️ Soʻrov yuborish',
                'Button Service price' => '💵 Xizmatlar narxi',
                'Button Settings' => '⚙️ Sozlamalar',
                'Button Yes' => '✔️ Ha',
                'Cancelled' => 'Bekor qilindi',
                'Cart' => 'Savat',
                'Cart is empty' => 'Savat boʻsh',
                'Catalog' => 'Katalog',
                'Check and cofirm your order' => 'Buyurtmani tekshiring va tasdiqlang',
                'Checkout' => 'Buyurtmani rasmiylashtirish',
                'Choose apartment' => 'Kvartirani tanlang',
                'Choose category' => 'Kategoriyani tanlang',
                'Choose delivery type 2' => 'Buyurtmani oʻzingiz olib keting yoki Yetkazib berishni tanlang',
                'Choose floor' => 'Qavatni tanlang',
                'Choose or write delivery method' => 'Yetkazib berish usulini tanlang yoki yozing',
                'Choose order' => 'Buyurtmani tanlang',
                'Choose payment method' => 'Toʻlov usulini tanlang',
                'Choose premises' => 'Joyni tanlang',
                'Choose product' => 'Mahsulotni tanlang',
                'Choose quantity' => 'Miqdorini tanlang',
                'Choose setting' => 'Sozlamani tanlang',
                'Code is incorrect' => 'Kod notoʻgʻri',
                'Commercial real estate' => 'Tijorat koʻchmas mulki',
                'Contact phone number' => 'Aloqa uchun telefon raqami',
                'Cost' => 'Narxi',
                'Currency sum' => 'soʻm',
                'Delivery' => 'Yetkazib berish',
                'Delivery self pickup' => 'Oʻzim olib ketaman',
                'Enter delivery address' => 'Yetkazib berish manzilini kiriting',
                'Enter delivery address or send location' => 'Yetkazib berish manzilini kiriting yoki lokatsiya yuboring',
                'Enter new address' => 'Yangi manzilni kiriting',
                'Enter new phone number' => 'Yangi telefon raqamini kiriting',
                'Get price' => 'Narxini bilish',
                'Go to cart' => 'Savatga oʻtish',
                'Go to orders' => 'Buyurtmalarim roʻyxati',
                'in format' => '%s formatda',
                'is dayoff' => 'ish kuni emas',
                'Last orders list' => 'Oxirgi buyurtmalaringiz roʻyxati',
                'Location' => 'Lokatsiya',
                'Menu' => 'Menyu',
                'Name' => 'Ismi',
                'Name of badges' => 'Наименование нагрудных знаков',
                'No information' => 'Ma\'lumot yoʻq',
                'No location' => 'Lokatsiya yoʻq',
                'No more products in the category. Return' => 'Bu kategoriyada boshqa mahsulot yoʻq. Orqaga qaytish',
                'No more products. Return last page' => 'Boshqa mahsulot yoʻq. Oxirgi sahifaga qaytish',
                'No orders' => 'Buyurtmalar yoʻq',
                'No products in cart' => 'Savat boʻsh',
                'New order' => 'Yangi buyurtma',
                'New request' => 'Yangi soʻrov',
                'Order accepted' => 'Buyurtma qabul qilindi',
                'Order cancelled' => 'Buyurtma bekor qilindi',
                'Order details' => 'Buyurtma tafsilotlari',
                'Order ID' => 'Buyurtma raqami',
                'Order not found' => 'Buyurtma topilmadi',
                'Order sum incorrect' => 'Buyurtma narxi notoʻgʻri',
                'Order are accepted from to' => 'Buyurtmalar %s dan %s gacha qabul qilinadi',
                'Our products' => 'Bizning mahsulotlarimiz',
                'Pay online' => 'Onlayn toʻlov',
                'Pay with Click' => 'Click bilan toʻlov',
                'Pay with Payme' => 'Payme bilan toʻlov',
                'Payment method' => 'Toʻlov usuli',
                'Phone number' => 'Telefon raqami',
                'Phone number saved' => 'Telefon raqami saqlandi',
                'Photos attached' => 'Фото прикреплено',
                'Premises' => 'Bino/Joy',
                'Price' => 'Narxi',
                'Product' => 'Mahsulot',
                'Product added to cart' => 'Mahsulot savatga qoʻshildi',
                'Products' => 'Mahsulotlar',
                'Quantity' => 'Miqdori',
                'Rate our services' => 'Xizmatlarimizni baholang',
                'Rating 1' => 'Juda yomon 👎🏻',
                'Rating 2' => 'Yomon ⭐️⭐️',
                'Rating 3' => 'Yoqmadi ⭐️⭐️⭐️',
                'Rating 4' => 'Yaxshi ⭐️⭐️⭐️⭐️',
                'Rating 5' => 'Hammasi yoqdi ♥️',
                'Request accepted' => 'Soʻrov qabul qilindi',
                'Request details' => 'Soʻrov tafsilotlari',
                'Residential real estate' => 'Turar-joy majmuasi',
                'Review accepted' => 'Rahmat! Fikringiz qabul qilindi',
                'Reviews' => 'Tavsiyalar',
                'Send location' => 'Lokatsiya yuboring',
                'Services cost' => 'Xizmatlar narxi',
                'Shipping price' => 'Yetkazib berish narxi',
                'Shipping price depends on location text' => 'Yetkazib berish narxi sizning manzilingizga bogʻliq va buyurtma tasdiqlangandan soʻng operator tomonidan xabar qilinadi',
                'Show products' => 'Mahsulotlarni koʻrish',
                'Sorry' => 'Kechirasiz',
                'Status' => 'Status',
                'To Order' => 'Buyurtma berish',
                'To Send location' => 'Lokatsiya yuborish',
                'Total' => 'Jami',
                'Welcome' => 'Xush kelibsiz!',
                'Write a review' => 'Fikr bildiring',
                'Write name of badges or send photo' => 'Если у вас есть фото прикрепите, затем напишите наименование нагрудных знаков. Пример: 25 лет ВС РУз, орден "Дружбы народов" , медаль " За отвагу".',
                'Your address' => 'Sizning manzilingiz',
                'Your name' => 'Ismingiz',
                'Your firstname' => 'Ismingiz',
                'Your lastname' => 'Familiyangiz',
                'Your phone number' => 'Sizning telefon raqamingiz',
                '' => '',
            ],
        ];
    }

    public static function loadRows($isThirdParty = false)
    {
        $rows = [
            ['company_code', 'Company code (' . implode(', ', Load::companyCodes()) . ')',],
            ['load_id', 'Load ID',],
            ['pcs_number', 'PCS number',],
            ['reference_no', 'Reference №',],
            ['rate', 'Rate',],
            ['authorized_loaded_mile', 'Authorized loaded mile',],
            ['deadhead_mile', 'Load DH mile',],
            ['authorized_mile', 'Authorized mile (PLD, TRL, FLT miles)',],
            ['unauthorized_mile', 'Unauthorized miles (including Home DH miles)',],
            ['pickup_location', 'PU address (full)',],
            // ['pickup_timezone', 'PU timezone (' . implode(', ', Load::timezones()) . ')',],
            ['local_pickup_time', 'PU time (yyyy-mm-dd hh:mm)',],
            ['pickup_time_for_driver', 'PU time for driver (yyyy-mm-dd hh:mm)',],
            ['delivery_location', 'Delivery address (full)',],
            // ['delivery_timezone', 'Delivery timezone (' . implode(', ', Load::timezones()) . ')',],
            ['local_delivery_time', 'Delivery time (yyyy-mm-dd hh:mm)',],
            ['delivery_time_for_driver', 'Delivery time for driver (yyyy-mm-dd hh:mm)',],
            ['trailer', 'Trailer',],
            ['trailer_note', 'Trailer note',],
            ['check_in_as', 'Check in as',],
            ['pickup_additional_stops', 'PU address and time (2, 3, 4 Stops)',],
            ['warnings', 'Warnings (specific)',],
        ];
        if ($isThirdParty) {
            $rows[] = ['third_company', 'Third company name',];
        } else {
            $rows[] = ['driver', 'Driver name',];
        }
        $rows[] = ['special_note', 'Special note',];
        return $rows;
    }

}
