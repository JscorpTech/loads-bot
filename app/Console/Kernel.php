<?php

namespace App\Console;

use App\Console\Commands\DailyReportCommand;
use App\Console\Commands\DbCleanupCommand;
use App\Console\Commands\DriverCreateWeeklyPlanCommand;
use App\Console\Commands\DriverPlanFillCommand;
use App\Console\Commands\DriverSendNotificationsCommand;
use App\Console\Commands\DriverStatusUpdateCommand;
use App\Console\Commands\DriverTeamUpdateCommand;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(DriverStatusUpdateCommand::class)->everyFiveMinutes();
        $schedule->command(DriverSendNotificationsCommand::class)->cron('1-59/5 * * * *');
        $schedule->command(DriverCreateWeeklyPlanCommand::class)->weeklyOn(0, '23:30');
        $schedule->command(DriverPlanFillCommand::class)->hourly();
        $schedule->command(DailyReportCommand::class)->dailyAt('07:00');
        $schedule->command(DbCleanupCommand::class)->dailyAt('15:00');
        $schedule->command(DriverTeamUpdateCommand::class)->everyThreeHours();
        // $schedule->command('inspire')->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
