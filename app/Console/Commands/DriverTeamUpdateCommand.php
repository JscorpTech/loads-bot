<?php

namespace App\Console\Commands;

use App\Models\Driver;
use Illuminate\Console\Command;

class DriverTeamUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:team_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Driver::active()->with('dispatcher.botUserInfo.team')->chunk(10, function ($drivers) {
            foreach ($drivers as $driver) {
                if (!empty($driver->dispatcher->botUserInfo->team) && $driver->dispatcher->botUserInfo->team->id != $driver->team_id) {
                    $driver->update([
                        'team_id' => $driver->dispatcher->botUserInfo->team->id,
                    ]);
                }
            }
        });
        return 0;
    }
}
