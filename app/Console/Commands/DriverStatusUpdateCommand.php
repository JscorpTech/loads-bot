<?php

namespace App\Console\Commands;

use App\Models\Driver;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class DriverStatusUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'driver:status_update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Driver status update';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Driver::active()->chunk(10, function($drivers){
            foreach ($drivers as $driver) {
                $driver->updateStatus();
            }
        });
        return 0;
    }
}
