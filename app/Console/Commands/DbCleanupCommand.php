<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DbCleanupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'app:db_cleanup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = now()->subDays(7);

        DB::table('bot_telegram_update')->whereNotNull('message_id')->whereIn('message_id', DB::table('bot_message')->select(['id'])->where('date', '<', $date)->pluck('id'))->delete();
        DB::table('bot_message')->where('date', '<', $date)->delete();

        DB::table('bot_telegram_update')->whereNotNull('edited_message_id')->whereIn('edited_message_id', DB::table('bot_edited_message')->select(['id'])->where('edit_date', '<', $date)->pluck('id'))->delete();
        DB::table('bot_edited_message')->where('edit_date', '<', $date)->delete();

        DB::table('bot_telegram_update')->whereNotNull('inline_query_id')->whereIn('inline_query_id', DB::table('bot_inline_query')->select(['id'])->where('created_at', '<', $date)->pluck('id'))->delete();
        DB::table('bot_inline_query')->where('created_at', '<', $date)->delete();

        DB::table('bot_telegram_update')->whereNotNull('chosen_inline_result_id')->whereIn('chosen_inline_result_id', DB::table('bot_chosen_inline_result')->select(['id'])->where('created_at', '<', $date)->pluck('id'))->delete();
        DB::table('bot_chosen_inline_result')->where('created_at', '<', $date)->delete();

        DB::table('bot_telegram_update')->whereNotNull('callback_query_id')->whereIn('callback_query_id', DB::table('bot_callback_query')->select(['id'])->where('created_at', '<', $date)->pluck('id'))->delete();
        DB::table('bot_callback_query')->where('created_at', '<', $date)->delete();

        DB::table('bot_conversation')->where('updated_at', '<', $date)->delete();
        DB::table('bot_request_limiter')->where('created_at', '<', $date)->delete();

        return 0;
    }
}
