<?php

namespace App\Console\Commands;

use App\Models\BotUserInfo;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DispatcherTeamUpdateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dispatcher_team:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $items = DB::table('dispatcher_teams')->get();
        foreach ($items as $item) {
            $this->info($item->dispatcher_id);
            $this->info($item->team_id);
            BotUserInfo::where('user_id', $item->dispatcher_id)->update(['team_id' => $item->team_id]);
        }
        return 0;
    }
}
