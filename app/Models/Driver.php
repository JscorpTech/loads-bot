<?php

namespace App\Models;

use App\Helpers\Helper;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class Driver extends Model
{
    use HasFactory;

    const W_TARGET = 10000;
    const REST_TIME = 180;

    const TYPE_COMPANY = '*C';
    const TYPE_OWNER = '*O';
    const TYPE_OWNER_OWNER = '*OO';
    const TYPE_RENTAL = '*R';
    const TYPE_LEASE = '*L';
    const TYPE_CP = '*CP';

    protected $guarded = [];

    protected $casts = [
        'birthday' => 'date',
    ];

    protected static function booted()
    {
        static::saving(function ($driver) {
            $addressParsed = Helper::parseAddress($driver->home_address);
            $driver->home_address_state_code = $addressParsed['state'];
            $driver->home_address_zip_code = $addressParsed['zip'];
        });
        static::saved(function ($driver) {
            if ($driver->isDirty('dispatcher_id')) {
                Changelog::create([
                    'driver_id' => $driver->id,
                    'dispatcher_id' => $driver->dispatcher_id,
                    'message' => __('New dispatcher') . ($driver->dispatcher ? ' ' . $driver->dispatcher->botUserInfo->full_name : ''),
                ]);
            }
        });
    }

    public function loads()
    {
        return $this->hasMany(Load::class);
    }

    public function driverStatuses()
    {
        return $this->hasMany(DriverStatus::class);
    }

    public function relatedload()
    {
        return $this->belongsTo(Load::class, 'load_id');
    }

    public function trailer()
    {
        return $this->belongsTo(Trailer::class);
    }

    public function driverPlans()
    {
        return $this->hasMany(DriverPlan::class);
    }

    public function dispatcher()
    {
        return $this->belongsTo(BotUser::class, 'dispatcher_id');
    }

    public function additionalDispatchers()
    {
        return $this->belongsToMany(BotUser::class, 'driver_dispatchers', 'driver_id', 'dispatcher_id');
    }

    public function getDispatcherNameAttribute()
    {
        $name = '';
        if ($this->dispatcher && $this->dispatcher->botUserInfo) {
            $name = $this->dispatcher->botUserInfo->full_name;
        }

        return $name;
    }

    public function getDispatchersNamesAttribute()
    {
        $names = [];
        if ($this->dispatcher) {
            $names[] = $this->dispatcher->botUserInfo->full_name;
        }
        foreach ($this->additionalDispatchers as $dispatcher) {
            $names[] = $dispatcher->botUserInfo->full_name;
        }

        return implode(', ', $names);
    }

    public function getAdditionalDispatchersNamesAttribute()
    {
        $names = [];
        foreach ($this->additionalDispatchers as $dispatcher) {
            if (!$dispatcher->botUserInfo) {
                continue;
            }
            $names[] = $dispatcher->botUserInfo->full_name;
        }
        return implode(', ', $names);
    }

    public function latestDriverStatus()
    {
        if (!$this->relationLoaded('driverStatuses')) {
            $this->load('driverStatuses');
        }
        $driverStatus = $this->driverStatuses->sortByDesc('id')->first();
        if (!$driverStatus) {
            $driverStatus = $this->driverStatuses()->create([
                'status' => DriverStatus::STATUS_READY_1,
                'start_time' => now(),
                'end_time' => now()->addHours(3),
            ]);
        }
        return $driverStatus;
    }

    public function currentDriverStatus()
    {
        return $this->belongsTo(DriverStatus::class, 'current_driver_status_id');
    }

    public function getCurrentDriverStatus(CarbonInterface $date = null)
    {
        if (!$this->relationLoaded('driverStatuses')) {
            $this->load(['driverStatuses' => function ($q) {
                $q->where('start_time', '>=', now()->subMonth())->with('relatedLoad');
            }]);
        }
        $current = false;
        if (!$date) {
            $current = true;
            $date = now();
        }

        $filtered = $this->driverStatuses->filter(function ($driverStatus) use ($date) {
            $isActive = false;
            if ($driverStatus->start_time <= $date && ($driverStatus->end_time >= $date || $driverStatus->end_time == null)) {
                $isActive = true;
            }
            return $isActive;
        });


        $driverStatus = $filtered->sortByDesc('id')->first();

        if (!$driverStatus && $current) {
            // check latest if ready 1 next is ready 2, elseif ready 2 next is ready3 else ready1
            $latestDriverStatus = $this->driverStatuses->sortByDesc('id')->first();
            $nextStatus = DriverStatus::STATUS_READY_1;
            $endTime = now()->addHours(3);
            $latestLoadId = !empty($latestDriverStatus->load_id) ? $latestDriverStatus->load_id : null;
            if ($latestDriverStatus && $latestDriverStatus->isReady1()) {
                $nextStatus = DriverStatus::STATUS_READY_2;
                $endTime = now()->addHours(2);
            } elseif ($latestDriverStatus && $latestDriverStatus->isReady2()) {
                $nextStatus = DriverStatus::STATUS_READY_3;
                $endTime = now()->addHours(6);
            } elseif ($latestDriverStatus && $latestDriverStatus->isReady3()) {
                $nextStatus = DriverStatus::STATUS_READY_3;
                $endTime = now()->addHours(6);
            }
            $driverStatus = $this->driverStatuses()->create([
                'load_id' => $latestLoadId,
                'status' => $nextStatus,
                'start_time' => now(),
                'end_time' => $endTime,
            ]);
        }
        return $driverStatus;
    }

    public function getStatusTitleAttribute()
    {
        return DriverStatus::statuses()[$this->status];
    }

    public function getStatusColorAttribute()
    {
        return DriverStatus::statusColors()[$this->status];
    }

    public function getNameAndIdAttribute()
    {
        return $this->name . ' (ID: ' . $this->id . ')';
    }

    public function updateStatus()
    {
        $now = now();
        $driverStatus = $this->getCurrentDriverStatus();

        if (!$driverStatus) {
            return;
        }

        if ($driverStatus->isShop()) {
            // nothing required, only driver status will be updated
        } elseif ($driverStatus->isRest() || $driverStatus->isHome() || $driverStatus->isVacation()) {
            // check current status ended and set next
            if ($driverStatus->end_time != null && $now > $driverStatus->end_time) {
                $upcomingLoad = $this->loads()->active()->where('pickup_time', '>=', $driverStatus->end_time)->orderBy('pickup_time')->first();
                // set next status
                if ($upcomingLoad && $upcomingLoad->pickup_time > $now) {
                    // next status dispatched
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'load_id' => $upcomingLoad->id,
                        'trailer_id' => $upcomingLoad->trailer_id,
                        'status' => DriverStatus::STATUS_DISPATCHED,
                        'start_time' => $driverStatus->end_time,
                        'end_time' => $upcomingLoad->pickup_time,
                    ]);
                } elseif ($upcomingLoad && $upcomingLoad->pickup_time <= $now) {
                    // next status transit
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'load_id' => $upcomingLoad->id,
                        'trailer_id' => $upcomingLoad->trailer_id,
                        'status' => DriverStatus::STATUS_TRANSIT,
                        'start_time' => $driverStatus->end_time,
                        'end_time' => $upcomingLoad->delivery_time,
                    ]);
                } else {
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'status' => DriverStatus::STATUS_READY_1,
                        'start_time' => $driverStatus->end_time,
                        'end_time' => (clone $driverStatus->end_time)->addHours(3),
                    ]);
                }
            }
        } elseif ($driverStatus->isReady()) {
            // check current status ended and set next
            $upcomingLoad = $this->loads()->active()->where('pickup_time', '>=', $driverStatus->end_time)->orderBy('pickup_time')->first();
            $currentLoad = $this->loads()->active()->where('pickup_time', '<=', now())->where('delivery_time', '>=', now())->orderBy('pickup_time')->first();

            // set next status
            if ($upcomingLoad) {
                $driverStatus->end_time = $now;
                $driverStatus->save();
                // next status dispatched
                if ($upcomingLoad->pickup_time > $now) {
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'load_id' => $upcomingLoad->id,
                        'trailer_id' => $upcomingLoad->trailer_id,
                        'status' => DriverStatus::STATUS_DISPATCHED,
                        'start_time' => $now,
                        'end_time' => $upcomingLoad->pickup_time,
                    ]);
                } else {
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'load_id' => $upcomingLoad->id,
                        'trailer_id' => $upcomingLoad->trailer_id,
                        'status' => DriverStatus::STATUS_TRANSIT,
                        'start_time' => $now,
                        'end_time' => $upcomingLoad->delivery_time,
                    ]);
                }
            } elseif ($currentLoad) {
                $driverStatus->end_time = $now;
                $driverStatus->save();
                $driverStatus = DriverStatus::firstOrCreate([
                    'driver_id' => $this->id,
                    'load_id' => $currentLoad->id,
                    'trailer_id' => $currentLoad->trailer_id,
                    'status' => DriverStatus::STATUS_TRANSIT,
                    'start_time' => $now,
                    'end_time' => $currentLoad->delivery_time,
                ]);
            } else {
                if ($driverStatus->isReady1() && $driverStatus->end_time < $now) {
                    $oldStatus = $driverStatus;
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'load_id' => !empty($oldStatus->load_id) ? $oldStatus->load_id : null,
                        'status' => DriverStatus::STATUS_READY_2,
                        'start_time' => $driverStatus->end_time,
                        'end_time' => (clone $driverStatus->end_time)->addHours(2),
                    ]);
                } elseif ($driverStatus->isReady2() && $driverStatus->end_time < $now) {
                    $oldStatus = $driverStatus;
                    $driverStatus = DriverStatus::firstOrCreate([
                        'driver_id' => $this->id,
                        'load_id' => !empty($oldStatus->load_id) ? $oldStatus->load_id : null,
                        'status' => DriverStatus::STATUS_READY_3,
                        'start_time' => $driverStatus->end_time,
                        'end_time' => null,
                    ]);
                }
            }
        } elseif ($driverStatus->isDispatched()) {
            // check changes
            if ($driverStatus->end_time > $now) {
                if ($driverStatus->relatedload && $driverStatus->relatedload->pickup_time != $driverStatus->end_time) {
                    $driverStatus->update([
                        'end_time' => $driverStatus->relatedload->pickup_time,
                    ]);
                }
            }
            // update status
            if ($driverStatus->end_time <= $now) {
                $load = $this->loads()->active()->where('pickup_time', '>=', $driverStatus->end_time)->orderBy('pickup_time')->first();
                $driverStatus = DriverStatus::firstOrCreate([
                    'driver_id' => $this->id,
                    'load_id' => $load->id,
                    'trailer_id' => $load->trailer_id,
                    'status' => DriverStatus::STATUS_TRANSIT,
                    'start_time' => $driverStatus->end_time,
                    'end_time' => $load->delivery_time,
                ]);
            }
        } elseif ($driverStatus->isTransit()) {
            // check changes
            if ($driverStatus->end_time > $now) {
                if ($driverStatus->relatedload && $driverStatus->relatedload->delivery_time != $driverStatus->end_time) {
                    $driverStatus->update([
                        'end_time' => $driverStatus->relatedload->delivery_time,
                    ]);
                }
            }
            // update status
            if ($driverStatus->end_time <= $now) {
                $oldStatus = $driverStatus;
                $driverStatus = DriverStatus::firstOrCreate([
                    'driver_id' => $this->id,
                    'load_id' => !empty($oldStatus->load_id) ? $oldStatus->load_id : null,
                    'status' => DriverStatus::STATUS_READY_1,
                    'start_time' => $driverStatus->end_time,
                    'end_time' => (clone $driverStatus->end_time)->addHours(3),
                ]);
            }
        }

        // update ready_tomorrow and ready_day_after_tomorrow
        $tomorrowStart = now()->addDay()->startOfDay();
        $tomorrowEnd = now()->addDay()->endOfDay();
        $tomorrow2Start = now()->addDays(2)->startOfDay();
        $tomorrow2End = now()->addDays(2)->endOfDay();
        $this->ready_tomorrow = 1;
        $this->ready_day_after_tomorrow = 1;

        // check by loads (tomorrow)
        $tomorrowLoads = Load::query()
            ->where('driver_id', $this->id)
            ->where(function ($q1) use ($tomorrowStart, $tomorrowEnd) {
                $q1
                    ->where(function ($q2) use ($tomorrowStart) {
                        $q2
                            ->where('pickup_time', '<=', $tomorrowStart)
                            ->where('delivery_time', '>=', $tomorrowStart);
                    })
                    ->orWhere(function ($q2) use ($tomorrowStart, $tomorrowEnd) {
                        $q2
                            ->where('pickup_time', '>=', $tomorrowStart)
                            ->where('pickup_time', '<=', $tomorrowEnd);
                    });
            })
            ->count();
        if ($tomorrowLoads > 0) {
            $this->ready_tomorrow = 0;
        }
        // check by statuses (tomorrow)
        $tomorrowDriverStatuses = DriverStatus::query()
            ->where('driver_id', $this->id)
            ->where(function ($q1) use ($tomorrowStart, $tomorrowEnd) {
                $q1
                    ->where(function ($q2) use ($tomorrowStart) {
                        $q2
                            ->where('start_time', '<=', $tomorrowStart)
                            ->where(function ($q3) use ($tomorrowStart) {
                                $q3
                                    ->where('end_time', '>=', $tomorrowStart)
                                    ->orWhereNull('end_time');
                            });
                    })
                    ->orWhere(function ($q2) use ($tomorrowStart, $tomorrowEnd) {
                        $q2
                            ->where('start_time', '>=', $tomorrowStart)
                            ->where('start_time', '<=', $tomorrowEnd);
                    });
            })
            ->get();
        foreach ($tomorrowDriverStatuses as $tomorrowDriverStatus) {
            if (in_array($tomorrowDriverStatus->status, DriverStatus::loadCreateUnavailableStatuses())) {
                $this->ready_tomorrow = 0;
            }
        }
        // check by loads (day_after_tomorrow)
        $dayAfterTomorrowLoads = Load::query()
            ->where('driver_id', $this->id)
            ->where(function ($q1) use ($tomorrow2Start, $tomorrow2End) {
                $q1
                    ->where(function ($q2) use ($tomorrow2Start) {
                        $q2
                            ->where('pickup_time', '<=', $tomorrow2Start)
                            ->where('delivery_time', '>=', $tomorrow2Start);
                    })
                    ->orWhere(function ($q2) use ($tomorrow2Start, $tomorrow2End) {
                        $q2
                            ->where('pickup_time', '>=', $tomorrow2Start)
                            ->where('pickup_time', '<=', $tomorrow2End);
                    });
            })
            ->count();
        if ($dayAfterTomorrowLoads > 0) {
            $this->ready_day_after_tomorrow = 0;
        }
        // check by statuses (day_after_tomorrow)
        $dayAfterTomorrowDriverStatuses = DriverStatus::query()
            ->where('driver_id', $this->id)
            ->where(function ($q1) use ($tomorrow2Start, $tomorrow2End) {
                $q1
                    ->where(function ($q2) use ($tomorrow2Start) {
                        $q2
                            ->where('start_time', '<=', $tomorrow2Start)
                            ->where(function ($q3) use ($tomorrow2Start) {
                                $q3
                                    ->where('end_time', '>=', $tomorrow2Start)
                                    ->orWhereNull('end_time');
                            });
                    })
                    ->orWhere(function ($q2) use ($tomorrow2Start, $tomorrow2End) {
                        $q2
                            ->where('start_time', '>=', $tomorrow2Start)
                            ->where('start_time', '<=', $tomorrow2End);
                    });
            })
            ->get();
        foreach ($dayAfterTomorrowDriverStatuses as $dayAfterTomorrowDriverStatus) {
            if (in_array($dayAfterTomorrowDriverStatus->status, DriverStatus::loadCreateUnavailableStatuses())) {
                $this->ready_day_after_tomorrow = 0;
            }
        }

        $this->trailer_id = $driverStatus->trailer_id;
        $this->load_id = $driverStatus->load_id;
        $this->status = $driverStatus->status;
        $this->current_driver_status_id = $driverStatus->id;
        $this->saveQuietly();
    }

    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    public function isActive()
    {
        return $this->active == 1;
    }

    public function getChatBindCodeAttribute()
    {
        return '/driverchat@' . config('services.telegram.bot_name') . ' ' . $this->id;
    }

    public function team()
    {
        return $this->belongsTo(Team::class, 'team_id');
    }

    public function getTeamNameAttribute()
    {
        $name = '-';
        if ($this->team) {
            $name = $this->team->name;
        }
        return $name;
    }

    public static function types() {
        return [
            static::TYPE_COMPANY => __('Company driver'),
            static::TYPE_OWNER => __('Owner'),
            static::TYPE_OWNER_OWNER => __('Owner Owner'),
            static::TYPE_RENTAL => __('Rental'),
            static::TYPE_LEASE => __('Lease'),
            static::TYPE_CP => __('CP driver'),
        ];
    }

    public function getTypeTitleAttribute()
    {
        return $this->type . ' - ' . (static::types()[$this->type] ?? '-');
    }

    public function isCompanyDriver()
    {
        return $this->type == static::TYPE_COMPANY;
    }
}
