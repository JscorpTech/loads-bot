<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotUser extends Model
{
    use HasFactory;

    protected $keyType = 'float';

    protected $guarded = [];

    protected $table = 'bot_user';

    public function botUserInfo()
    {
        return $this->hasOne(BotUserInfo::class, 'user_id');
        // return BotUserInfo::where('user_id', (string)$this->id)->first();
    }

    public function dspLoads()
    {
        $this->hasMany(Load::class, 'dsp_bot_user_id');
    }

    public function qcLoads()
    {
        $this->hasMany(Load::class, 'qc_bot_user_id');
    }

    public function drivers()
    {
        return $this->hasMany(Driver::class, 'dispatcher_id');
    }

    public function additionalDrivers()
    {
        return $this->belongsToMany(Driver::class, 'driver_dispatchers', 'dispatcher_id', 'driver_id')->withPivot(['sort_number']);
    }

    public function getNameAttribute()
    {
        $name = $this->first_name;
        if ($this->last_name) {
            $name .= ' ' . $this->last_name;
        }
        return $name;
    }
}
