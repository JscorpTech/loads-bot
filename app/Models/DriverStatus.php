<?php

namespace App\Models;

use App\Helpers\Helper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverStatus extends Model
{
    use HasFactory;

    const STATUS_SHOP = -1;
    const STATUS_HOME = 0;
    const STATUS_DISPATCHED = 1;
    const STATUS_TRANSIT = 2;
    const STATUS_REST = 3;
    const STATUS_READY_1 = 4;
    const STATUS_READY_2 = 5;
    const STATUS_READY_3 = 6;
    const STATUS_VACATION = 7;

    protected $guarded = [];

    protected $casts = [
        'start_time' => 'datetime',
        'end_time' => 'datetime',
    ];

    protected static function booted()
    {
        static::saving(function ($driverStatus) {
            $addressParsed = Helper::parseAddress($driverStatus->shop_address);
            $driverStatus->shop_address_state_code = $addressParsed['state'];
            $driverStatus->shop_address_zip_code = $addressParsed['zip'];
        });
    }

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

    public function relatedload()
    {
        return $this->belongsTo(Load::class, 'load_id');
    }

    public static function defaultStatus()
    {
        return static::STATUS_READY_1;
    }

    public function getStatusTitleAttribute()
    {
        return static::statuses()[$this->status] ?? static::statuses()[static::defaultStatus()];
    }

    public function getStatusColorAttribute()
    {
        return static::statusColors()[$this->status] ?? static::statusColors()[static::defaultStatus()];
    }

    public static function statuses()
    {
        return [
            static::STATUS_SHOP => __('Shop'),
            static::STATUS_HOME => __('Home'),
            static::STATUS_DISPATCHED => __('Dispatched'),
            static::STATUS_TRANSIT => __('Transit'),
            static::STATUS_REST => __('Rest'),
            static::STATUS_READY_1 => __('Ready 1'),
            static::STATUS_READY_2 => __('Ready 2'),
            static::STATUS_READY_3 => __('Ready 3'),
            static::STATUS_VACATION => __('Vacation'),
        ];
    }

    public static function statusColors()
    {
        return [
            static::STATUS_SHOP => '#EA7100',
            static::STATUS_HOME => '#AF241A',
            static::STATUS_DISPATCHED => '#377CD5',
            static::STATUS_TRANSIT => '#25538F',
            static::STATUS_REST => '#EB3223',
            static::STATUS_READY_1 => '#49742C',
            static::STATUS_READY_2 => '#345320',
            static::STATUS_READY_3 => '#000000',
            static::STATUS_VACATION => '#B21B45',
        ];
    }

    public static function statusIcons()
    {
        return [
            static::STATUS_SHOP => '<i class="fa-solid fa-circle-xmark"></i>',
            static::STATUS_HOME => '<i class="fa-solid fa-circle-xmark"></i>',
            static::STATUS_DISPATCHED => '<i class="fa-solid fa-clock"></i>',
            static::STATUS_TRANSIT => '<i class="fa-solid fa-circle-check"></i>',
            static::STATUS_REST => '<i class="fa-solid fa-circle-xmark"></i>',
            static::STATUS_READY_1 => '<i class="fa-solid fa-circle-exclamation"></i>',
            static::STATUS_READY_2 => '<i class="fa-solid fa-circle-exclamation"></i>',
            static::STATUS_READY_3 => '<i class="fa-solid fa-circle-exclamation"></i>',
            static::STATUS_VACATION => '<i class="fa-solid fa-circle-xmark"></i>',
        ];
    }

    public function isShop()
    {
        return $this->status == static::STATUS_SHOP;
    }

    public function isRest()
    {
        return $this->status == static::STATUS_REST;
    }

    public function isHome()
    {
        return $this->status == static::STATUS_HOME;
    }

    public function isVacation()
    {
        return $this->status == static::STATUS_VACATION;
    }

    public function isReady()
    {
        return in_array($this->status, [static::STATUS_READY_1, static::STATUS_READY_2, static::STATUS_READY_3]);
    }

    public function isReady1()
    {
        return $this->status == static::STATUS_READY_1;
    }

    public function isReady2()
    {
        return $this->status == static::STATUS_READY_2;
    }

    public function isReady3()
    {
        return $this->status == static::STATUS_READY_3;
    }

    public function isDispatched()
    {
        return $this->status == static::STATUS_DISPATCHED;
    }

    public function isTransit()
    {
        return $this->status == static::STATUS_TRANSIT;
    }

    public static function loadCreateUnavailableStatuses()
    {
        return [
            static::STATUS_SHOP,
            static::STATUS_HOME,
            static::STATUS_REST,
            static::STATUS_VACATION,
        ];
    }
}
