<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasFactory, HasApiTokens;

    const ROLE_ADMIN = 1;
    const ROLE_DSP = 2;
    const ROLE_QC = 3;
    const ROLE_MANAGER = 4;
    const ROLE_USER = 5;
    const ROLE_INS = 6;
    const ROLE_FLT = 7;
    const ROLE_TRL = 8;
    const ROLE_EQ = 9;
    const ROLE_ACCT = 10;
    const ROLE_SFT = 11;
    const ROLE_ELD = 12;
    const ROLE_IT = 13;
    const ROLE_MKTG = 14;
    const ROLE_OPS = 15;
    const ROLE_HR = 16;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone_number', 'role_id', 'bot_user_info_id', 'active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function payments()
    {
        return $this->hasMany(Payment::class);
    }

    public static function roles()
    {
        return [
            static::ROLE_ADMIN => __('Admin'),
            static::ROLE_DSP => __('DSP'),
            static::ROLE_QC => __('QC'),
            static::ROLE_MANAGER => __('Manager'),
            static::ROLE_USER => __('User'),
            static::ROLE_INS => __('Insurance'),
            static::ROLE_FLT => __('Fleet'),
            static::ROLE_TRL => __('Trailer'),
            static::ROLE_EQ => __('Equipment'),
            static::ROLE_ACCT => __('Accounting'),
            static::ROLE_SFT => __('Safety'),
            static::ROLE_ELD => __('Safety ELD'),
            static::ROLE_IT => __('Information Technologies'),
            static::ROLE_MKTG => __('Marketing'),
            static::ROLE_OPS => __('Operations managers'),
            static::ROLE_HR => __('Human resources'),
        ];
    }

    public function isAdmin()
    {
        return $this->role_id == static::ROLE_ADMIN;
    }

    public function isManager()
    {
        return $this->role_id == static::ROLE_MANAGER;
    }

    public function isDsp()
    {
        return $this->role_id == static::ROLE_DSP;
    }

    public function isQc()
    {
        return $this->role_id == static::ROLE_QC;
    }

    public function isUser()
    {
        return $this->role_id == static::ROLE_USER;
    }

    public function botUserInfo() {
        return $this->belongsTo(BotUserInfo::class);
    }

    public function getRoleNameAttribute()
    {
        return static::roles()[$this->role_id] ?? __('No role');
    }

    public function hasPermission($permission)
    {
        if ($this->isAdmin()) {
            return true;
        }
        $permissions = [
            static::ROLE_DSP => [
                'view_dsp_kpi_board'
            ]
        ];
        if (empty($permissions[$this->role_id])) {
            return false;
        }
        return in_array($permission, $permissions[$this->role_id]);
    }
}
