<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

class ArtisanController extends Controller
{
    public function migrate()
    {
        Artisan::call('migrate', [
            '--force' => true
         ]);
    }

    public function driverStatusUpdate()
    {
        Artisan::call('driver:status_update');
    }

    public function driverCreateWeeklyPlan()
    {
        Artisan::call('driver:create_weekly_plan');
    }
}
