<?php

namespace App\Http\Controllers;

use App\Models\BotUserInfo;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class BotUserInfoController extends Controller
{
    public function index(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = $this->generateQuery($request);
        $botUserInfos = $query->orderBy('full_name')->paginate(100);
        $statuses = BotUserInfo::statuses();
        $positions = BotUserInfo::positions();

        $qcDayQty = BotUserInfo::where('position', BotUserInfo::POSITION_QC)->where('shift', BotUserInfo::SHIFT_DAY)->count();
        $dspDayQty = BotUserInfo::where('position', BotUserInfo::POSITION_DSP)->where('shift', BotUserInfo::SHIFT_DAY)->count();
        $qcNightQty = BotUserInfo::where('position', BotUserInfo::POSITION_QC)->where('shift', BotUserInfo::SHIFT_NIGHT)->count();
        $dspNightQty = BotUserInfo::where('position', BotUserInfo::POSITION_DSP)->where('shift', BotUserInfo::SHIFT_NIGHT)->count();
        $stats = [
            'all' => $qcDayQty + $dspDayQty + $qcNightQty + $dspNightQty,
            'dsp' => $dspDayQty + $dspNightQty,
            'qc' => $qcDayQty + $qcNightQty,
            'dsp_day' => $dspDayQty,
            'dsp_night' => $dspNightQty,
            'qc_day' => $qcDayQty,
            'qc_night' => $qcNightQty,
        ];
        return view('bot-user-info.index', compact('botUserInfos', 'statuses', 'positions', 'filter', 'stats'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $botUserInfo = BotUserInfo::findOrFail($id);
        $positions = BotUserInfo::positions();
        $shifts = BotUserInfo::shifts();
        $teams = Team::all();
        return view('bot-user-info.edit', compact('botUserInfo', 'positions', 'shifts', 'teams'));
    }

    public function update(Request $request, $id)
    {
        $data = $this->validatedData($request);
        $data['active'] = !empty($data['active']) ? 1 : 0;
        $data['dsp_manager'] = !empty($data['dsp_manager']) ? 1 : 0;
        $data['qc_manager'] = !empty($data['qc_manager']) ? 1 : 0;
        $data['relations_manager'] = !empty($data['relations_manager']) ? 1 : 0;
        $data['trailers_manager'] = !empty($data['trailers_manager']) ? 1 : 0;
        $data['trucks_manager'] = !empty($data['trucks_manager']) ? 1 : 0;

        $botUserInfo = BotUserInfo::findOrFail($id);
        $botUserInfo->update($data);
        return redirect()->route('bot-user-info.index')->with('success', 'User saved');
    }

    public function destroy($id)
    {
        $botUserInfo = BotUserInfo::findOrFail($id);
        $botUserInfo->delete();
        return redirect()->route('bot-user-info.index')->with('success', 'User deleted');
    }

    private function validatedData(Request $request, $options = [])
    {
        $rules = [
            'full_name' => 'required|max:255',
            'position' => 'required|in:-,' . implode(',', BotUserInfo::positions()),
            'shift' => 'required|in:-,' . implode(',', BotUserInfo::shifts()),
            'team_id' => 'nullable|exists:teams,id',
            'active' => '',
            'dsp_manager' => '',
            'qc_manager' => '',
            'relations_manager' => '',
            'trailers_manager' => '',
            'trucks_manager' => '',
            'load_types' => 'array',
            'company_codes' => 'array',
        ];
        $rules = array_merge($rules, $options);
        return $request->validate($rules, [
            '*.required' => __('Required field'),
        ]);
    }

    private function generateQuery(Request $request)
    {
        $filter = $this->getFilter($request);
        $query = BotUserInfo::query();
        foreach ($filter as $key => $value) {
            if ($value === '' || $value === null || $value === '-') {
                continue;
            }
            if (in_array($key, ['full_name'])) {
                $query->where($key, 'LIKE', '%' . $value . '%');
            }
            if (in_array($key, ['status', 'active', 'position'])) {
                $query->where($key, $value);
            }
            if (in_array($key, ['username'])) {
                $query->whereHas('botUser', function($q) use ($key, $value) {
                    $q->where($key, 'LIKE', '%' . $value . '%');
                });
            }
        }
        $query->with(['botUser']);
        return $query;
    }

    private function getFilter(Request $request)
    {
        $filter = [
            'full_name' => $request->input('full_name', ''),
            'username' => $request->input('username', ''),
            'active' => $request->input('active', '-'),
            'position' => $request->input('position', '-'),
        ];
        return $filter;
    }
}
