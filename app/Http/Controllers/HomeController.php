<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\DriverPlan;
use App\Models\Team;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function board(Request $request)
    {
        $filter = [];

        $filter['dispatcher_id'] = $request->input('dispatcher_id', '');
        $filter['team_id'] = $request->input('team_id', '');
        $teams = Team::orderBy('name')->get();
        $dispatchersListQuery = BotUserInfo::dispatchers()->active();
        if ($filter['team_id']) {
            $dispatchersListQuery->where('team_id', $filter['team_id']);
        }
        $dispatchersList = $dispatchersListQuery->orderBy('full_name')->get();

        // start query
        $query = Driver::active()->with('currentDriverStatus');

        if ($filter['team_id']) {
            $query->whereHas('dispatcher', function ($q1) use ($filter) {
                $q1->whereHas('botUserInfo', function ($q2) use ($filter) {
                    $q2->where('team_id', $filter['team_id']);
                });
            });
        }
        if ($filter['dispatcher_id'] && $dispatchersList->contains('id', $filter['dispatcher_id'])) {
            $query->whereHas('dispatcher', function ($q1) use ($filter) {
                $q1->whereHas('botUserInfo', function ($q2) use ($filter) {
                    $q2->where('id', $filter['dispatcher_id']);
                });
            });
        }

        $query->with(['dispatcher.botUserinfo', 'relatedLoad', 'trailer']);

        $cacheKey = 'main-board';
        foreach ($filter as $key => $value) {
            $cacheKey .= '-' . $key . '-' . $value;
        }
        session()->put('latest-cache-key', $cacheKey);
        $drivers = Cache::remember($cacheKey, now()->addSeconds(30), function () use ($query) {
            return $query->orderBy('name')->get();
        });

        return view('board', compact('drivers', 'dispatchersList', 'teams', 'filter'));
    }

    public function teamSheet(Request $request)
    {
        $filter = [];

        $filter['dispatcher_id'] = $request->input('dispatcher_id', '');
        $filter['team_id'] = $request->input('team_id', '');
        $teams = Team::orderBy('name')->get();
        $dispatchersListQuery = BotUserInfo::dispatchers()->active();
        if ($filter['team_id']) {
            $dispatchersListQuery->where('team_id', $filter['team_id']);
        }
        $dispatchersList = $dispatchersListQuery->orderBy('full_name')->get();

        // start query
        $query = BotUserInfo::dispatchers();

        if ($filter['team_id']) {
            $query->where('team_id', $filter['team_id']);
        }
        if ($filter['dispatcher_id'] && $dispatchersList->contains('id', $filter['dispatcher_id'])) {
            $query->where('id', $filter['dispatcher_id']);
        }

        $query->with(['botUser' => function ($q1) {
            $q1->with(['drivers' => function ($q2) {
                $q2->active()->with(['dispatcher.botUserinfo', 'relatedLoad', 'trailer', 'currentDriverStatus'])->orderBy('drivers.id', 'desc');
            }]);
        }]);

        $cacheKey = 'main-team-sheet';
        foreach ($filter as $key => $value) {
            $cacheKey .= '-' . $key . '-' . $value;
        }
        session()->put('latest-cache-key', $cacheKey);
        $dispatchers = Cache::remember($cacheKey, now()->addSeconds(30), function () use ($query) {
            return $query->orderBy('full_name')->get();
        });

        return view('team-sheet', compact('dispatchers', 'dispatchersList', 'teams', 'filter'));
    }

    public function kpiBoard(Request $request)
    {
        $filter = [];
        $user = auth()->user();
        $teams = Team::orderBy('name')->get();
        $filter['shift'] = $request->input('shift', '');
        $filter['dispatcher_id'] = $request->input('dispatcher_id', '');
        $defaultTeamId = session()->has('kpi_board_team_id') ? session()->get('kpi_board_team_id') : (auth()->user()->botUserInfo ? auth()->user()->botUserInfo->team_id : '');
        $filter['team_id'] = $request->input('team_id', $defaultTeamId);
        session()->put('kpi_board_team_id', $filter['team_id']);
        $dispatchersListQuery = BotUserInfo::dispatchers()->active();
        if ($filter['team_id']) {
            $dispatchersListQuery->where('team_id', $filter['team_id']);
        }
        $dispatchersList = $dispatchersListQuery->orderBy('full_name')->get();
        $shifts = BotUserInfo::shifts();

        $currentYear = date('Y');
        $filter['year'] = $request->input('year', $currentYear);
        try {
            $date = Carbon::createFromDate($filter['year']);
        } catch (\Throwable $th) {
            abort(400);
        }

        $weeks = [];
        for ($i = 1; $i <= $date->weeksInYear; $i++) {
            $weeks[] = $i;
        }
        $now = now();
        $currentWeek = $now->weekOfYear;
        $filter['week'] = $request->input('week', $currentWeek);
        if ($filter['week'] < 1) {
            $filter['week'] = 1;
        } elseif ($filter['week'] > $date->weeksInYear) {
            $filter['week'] = $date->weeksInYear;
        }

        $years = [];
        for ($i = $currentYear; $i >= config('services.cargoprime.start_year'); $i--) {
            $years[] = $i;
        }
        try {
            $weekStart = now()->setISODate($filter['year'], $filter['week'])->startOfWeek();
        } catch (\Throwable $th) {
            abort(400);
        }
        $weekEnd = (clone $weekStart)->endOfWeek();

        // start query
        $query = BotUserInfo::dispatchers();

        if ($filter['team_id']) {
            $query->where('team_id', $filter['team_id']);
        }
        if ($filter['dispatcher_id'] && $dispatchersList->contains('id', $filter['dispatcher_id'])) {
            $query->where('id', $filter['dispatcher_id']);
        }
        if ($filter['shift']) {
            $query->where('shift', $filter['shift']);
        }

        $query->with(['botUser' => function ($q1) use ($weekStart, $weekEnd) {
            $q1->with(['drivers' => function ($q2) use ($weekStart, $weekEnd) {
                $q2->active()->with([
                    'loads' => function ($q3) use ($weekStart, $weekEnd) {
                        $q3->active()->where('pickup_time', '>=', $weekStart)->where('pickup_time', '<=', $weekEnd);
                    },
                    'driverPlans' => function ($q3) use ($weekStart, $weekEnd) {
                        $q3->where('start_time', $weekStart->format('Y-m-d H:i:s'));
                    },
                    'driverStatuses' => function ($q3) use ($weekStart, $weekEnd) {
                        $q3
                            ->where(function ($q4) use ($weekStart, $weekEnd) {
                                $q4
                                    ->where(function ($q5) use ($weekStart) {
                                        $q5
                                            ->where('driver_statuses.start_time', '<=', $weekStart)
                                            ->where(function ($q6) use ($weekStart) {
                                                $q6
                                                    ->where('driver_statuses.end_time', '>=', $weekStart)
                                                    ->orWhereNull('driver_statuses.end_time');
                                            });
                                    })
                                    ->orWhere(function ($q5) use ($weekStart, $weekEnd) {
                                        $q5
                                            ->where('driver_statuses.start_time', '>=', $weekStart)
                                            ->where('driver_statuses.start_time', '<=', $weekEnd);
                                    });
                            });
                    },
                    'currentDriverStatus',
                ])->orderBy('id', 'desc');
            }]);
        }]);

        $cacheKey = 'kpi-board';
        foreach ($filter as $key => $value) {
            $cacheKey .= '-' . $key . '-' . $value;
        }
        session()->put('latest-cache-key', $cacheKey);
        $dispatchers = Cache::remember($cacheKey, now()->addMinutes(5), function () use ($query) {
            return $query->orderBy('full_name')->get();
        });

        // dd($dispatchers->first()->botUser->drivers->first());

        if ($request->has('download')) {
            $writer = WriterEntityFactory::createXLSXWriter();

            $fileName = 'KPI-board-' . $weekStart->format('Y-m-d') . '-' . $weekEnd->format('Y-m-d') . '.xlsx';
            $writer->openToBrowser($fileName); // stream data directly to the browser

            $values = [
                'KPI board'
            ];
            $rowFromValues = WriterEntityFactory::createRowFromArray($values);
            $writer->addRow($rowFromValues);
            $teamsList = $teams;
            if ($filter['team_id']) {
                $teamsList = $teamsList->where('id', $filter['team_id']);
            }
            foreach ($teamsList as $team) {
                $dispatchersList = $dispatchers->where('team_id', $team->id);
                foreach ($dispatchersList as $dispatcher) {
                    $newSheet = $writer->addNewSheetAndMakeItCurrent();
                    $newSheet->setName(mb_substr($dispatcher->id . ' ' . preg_replace('/[^A-Za-z0-9 ]/', '', $dispatcher->full_name), 0, 31));
                    if ($dispatcher->botUser && $dispatcher->botUser->drivers->isNotEmpty()) {
                        // add days
                        $values = [
                            '',
                        ];
                        for ($i = 0; $i < 7; $i++) {
                            $day = (clone $weekStart)->addDays($i);
                            $values[] = Helper::formatDate($day);
                        }
                        $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                        $writer->addRow($rowFromValues);

                        foreach ($dispatcher->botUser->drivers as $driver) {
                            $weeklyStats = Helper::getDriverWeeklyStats($driver, $weekStart, $weekEnd);
                            $dailyStats = [];
                            for ($i = 0; $i < 7; $i++) {
                                $dailyStats[$i] = Helper::getDriverDailyStats($driver, (clone $weekStart)->addDays($i), $weeklyStats['loads']);
                            }
                            // row
                            $values = [];
                            $driverInfo = $driver->name . "\n" .
                                $driver->status_title . "\n" .
                                __('Weekly target') . ": " . Helper::formatPrice($weeklyStats['weeklyTarget']) . ' / ' . $weeklyStats['weeklyTargetMile'] . ' mile' . "\n" .
                                __('Total $ (active loads rate + cancelled loads TONU)') . ': ' . Helper::formatPrice($weeklyStats['loadsTotal']) . ' | ' . $weeklyStats['completedPercent'] . '%' . "\n" .
                                __('Total mile') . ': ' . $weeklyStats['loadsMile'] . "\n" .
                                __('Per mile') . ': ' . Helper::formatPrice($weeklyStats['perMile']) . "\n";
                            if (auth()->user()->isAdmin()) {
                                $driverInfo .= __('Profit') . ': ' . Helper::formatPrice($weeklyStats['profit']) . "\n";
                            }
                            $values[] = $driverInfo;
                            for ($i = 0; $i < 7; $i++) {
                                $dayInfo = __('Total $') . ': ' . Helper::formatPrice($dailyStats[$i]['loadsTotal']) . "\n" .
                                    __('Total mile') . ': ' . $dailyStats[$i]['loadsMile'] . "\n" .
                                    __('Per mile') . ': ' . Helper::formatPrice($dailyStats[$i]['perMile']) . "\n";
                                if ($dailyStats[$i]['loads']->isNotEmpty()) {
                                    $dayInfo .= __('Loads') . ': ' . "\n";
                                    foreach ($dailyStats[$i]['loads'] as $load) {
                                        $dayInfo .= $load->load_id . ($load->isCancelled() ? '(Cancelled)' : '') . "\n";
                                    }
                                }

                                $values[] = $dayInfo;
                            }
                            $rowFromValues = WriterEntityFactory::createRowFromArray($values);
                            $writer->addRow($rowFromValues);
                        }
                    }
                }
            }

            $writer->close();
            exit();
        }

        return view('kpi-board', compact('user', 'dispatchers', 'dispatchersList', 'teams', 'shifts', 'filter', 'years', 'weeks', 'weekStart', 'weekEnd'));
    }

    public function driversBoard(Request $request)
    {
        $filter = [];
        $filter['dispatcher_id'] = $request->input('dispatcher_id', '');
        $filter['team_id'] = $request->input('team_id', '');
        $teams = Team::orderBy('name')->get();
        $dispatchersListQuery = BotUserInfo::dispatchers()->active();
        if ($filter['team_id']) {
            $dispatchersListQuery->where('team_id', $filter['team_id']);
        }
        $dispatchersList = $dispatchersListQuery->orderBy('full_name')->get();

        $currentYear = date('Y');
        $filter['year'] = $request->input('year', $currentYear);
        try {
            $date = Carbon::createFromDate($filter['year']);
        } catch (\Throwable $th) {
            abort(400);
        }

        $weeks = [];
        for ($i = 1; $i <= $date->weeksInYear; $i++) {
            $weeks[] = $i;
        }
        $now = now();
        $currentWeek = $now->weekOfYear;
        $filter['week'] = $request->input('week', $currentWeek);
        if ($filter['week'] < 1) {
            $filter['week'] = 1;
        } elseif ($filter['week'] > $date->weeksInYear) {
            $filter['week'] = $date->weeksInYear;
        }

        $years = [];
        for ($i = $currentYear; $i >= config('services.cargoprime.start_year'); $i--) {
            $years[] = $i;
        }
        try {
            $weekStart = now()->setISODate($filter['year'], $filter['week'])->startOfWeek();
        } catch (\Throwable $th) {
            abort(400);
        }
        $weekEnd = (clone $weekStart)->endOfWeek();

        // start query
        $query = DriverPlan::where('start_time', $weekStart->format('Y-m-d H:i:s'))->with('driver.dispatcher.botUserInfo');

        $query->whereHas('driver', function ($q1) use ($filter, $dispatchersList) {
            $q1->whereHas('dispatcher', function ($q2) use ($filter, $dispatchersList) {
                $q2->whereHas('botUserInfo', function ($q3) use ($filter, $dispatchersList) {
                    if ($filter['team_id']) {
                        $q3->where('team_id', $filter['team_id']);
                    }
                    if ($filter['dispatcher_id'] && $dispatchersList->contains('id', $filter['dispatcher_id'])) {
                        $q3->where('id', $filter['dispatcher_id']);
                    }
                });
            });
        });

        $cacheKey = 'driver-board';
        foreach ($filter as $key => $value) {
            $cacheKey .= '-' . $key . '-' . $value;
        }
        session()->put('latest-cache-key', $cacheKey);
        $driverPlans = Cache::remember($cacheKey, now()->addMinutes(5), function () use ($query) {
            return $query->orderBy('mile', 'desc')->get();
        });

        return view('drivers-board', compact('driverPlans', 'dispatchersList', 'teams', 'filter', 'years', 'weeks', 'weekStart', 'weekEnd'));
    }
}
