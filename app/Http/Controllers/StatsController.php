<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Models\BotUserInfo;
use App\Models\Driver;
use App\Models\DriverPlan;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Cache;

class StatsController extends Controller
{
    public function drivers(Request $request)
    {
        $filter = [];
        $currentYear = date('Y');
        $filter['year'] = $request->input('year', $currentYear);
        try {
            $date = Carbon::createFromDate($filter['year']);
        } catch (\Throwable $th) {
            abort(400);
        }
        $weeks = [];
        for ($i = 1; $i <= $date->weeksInYear; $i++) {
            $weeks[] = $i;
        }
        $now = now();
        $currentWeek = $now->weekOfYear;
        $filter['week'] = $request->input('week', $currentWeek);
        if ($filter['week'] < 1) {
            $filter['week'] = 1;
        } elseif ($filter['week'] > $date->weeksInYear) {
            $filter['week'] = $date->weeksInYear;
        }
        $years = [];
        for ($i = $currentYear; $i >= config('services.cargoprime.start_year'); $i--) {
            $years[] = $i;
        }
        try {
            $weekStart = now()->setISODate($filter['year'], $filter['week'])->startOfWeek();
        } catch (\Throwable $th) {
            abort(400);
        }
        $weekEnd = (clone $weekStart)->endOfWeek();

        $drivers = Driver::active()->with(['loads' => function ($q)  use ($weekStart, $weekEnd) {
            $q->active()->where('pickup_time', '>=', $weekStart)->where('pickup_time', '<=', $weekEnd);
        }])->orderBy('id', 'desc')->paginate(50);

        return view('stats.drivers', compact('drivers', 'filter', 'years', 'weeks', 'weekStart', 'weekEnd'));
    }
}
