<?php

namespace App\Http\Controllers;

use App\Models\Trailer;
use Illuminate\Http\Request;

class TrailerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $trailers = Trailer::orderBy('trailer_number')->get();

        return view('trailers.index', compact('trailers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $trailer = new Trailer();
        return view('trailers.create', compact('trailer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validatedData($request);
        $trailer = Trailer::create($data);
        return redirect()->route('trailers.index')->with('success', 'Trailer saved');
    }

    /**
     * Display the specified resource.
     *
     * @param int
     * @return \Illuminate\Http\Response
     */
    public function show(Trailer $trailer)
    {
        return view('trailers.show', compact('trailer'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $trailer
     * @return \Illuminate\Http\Response
     */
    public function edit(Trailer $trailer)
    {
        return view('trailers.edit', compact('trailer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $trailer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Trailer $trailer)
    {
        $data = $this->validatedData($request, $trailer);
        $trailer->update($data);
        return redirect()->route('trailers.index')->with('success', 'Trailer saved');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $trailer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Trailer $trailer)
    {
        $trailer->delete();
        return redirect()->route('trailers.index')->with('success', 'Trailer deleted');
    }

    private function validatedData(Request $request, Trailer $trailer = null)
    {
        $validation = [
            'trailer_number' => 'required|max:255|unique:trailers,trailer_number',
        ];
        if ($trailer) {
            $validation['trailer_number'] .= ',' . $trailer->id;
        }
        return $request->validate($validation);
    }
}
