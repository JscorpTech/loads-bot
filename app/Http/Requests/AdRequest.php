<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'description' => 'required|max:50000',
            'days_of_month' => 'nullable|array',
            'days_of_month.*' => 'numeric',
            'days_of_week' => 'nullable|array',
            'days_of_week.*' => 'numeric',
            'hours' => 'required|integer|between:0,23',
            'minutes' => 'required|integer|between:0,59',
            'images' => 'nullable|array',
        ];
    }
}
